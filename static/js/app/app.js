// load language
$.ajax({method: 'GET', url: './static/js/app/i18n/' + Ember.util.getLanguage() + '.js', async: false }).done(function (response) {
    try{
        eval(response);
    } catch(e) {
        console.log('Error parse language file: ', './static/js/app/i18n/' + Ember.util.getLanguage() + '.js');
        console.exception(e);
    }
    Ember.util.initDB();

    window.App = Ember.Application.create({
        currentPath: '',
        LOG_TRANSITIONS: false
    });

    // for register components (it is need because Ember register automatically if only template in DOM before init App)
    App.registerComponent = function (name) {
        var fullName = 'component:' + name,
            templateFullName = 'template:components/' + name;
        App.__container__.injection(fullName, 'layout', templateFullName);
        var Component = App.__container__.lookupFactory(fullName);
        if (!Component) {
            App.__container__.register(fullName, Ember.Component);
            Component = App.__container__.lookupFactory(fullName);
        }
        Ember.Handlebars.helper(name, Component);
    }
    // Вставляются скрипты после сборки
    {{scripts}}
    // проверка авторизован ли пользователь
    var from = location.hash.slice(1);
    if(from == '/auth') return;
    location.hash = '/auth';
    var hasUser = Ember.util.getCookie('user');
    if(!hasUser) return;
    try{
       App.User = JSON.parse(CryptoJS.enc.Base64.parse(hasUser).toString(CryptoJS.enc.Utf8));
    } catch(e){
        return;
    }
    App.User = App.UserModel.create(App.User);
    App.User.updateSecret().then(function(){
        $('head').append('<script type="application/javascript" src="/static/js/panel.js"></script>');
        if(from.indexOf('panel')>=0){
            location.hash = from;
        } else location.hash = '#/panel';
    });
});

