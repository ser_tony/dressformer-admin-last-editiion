Em.I18n.translations = {
    'btnIndicatorLoading': 'Loading...',

    'app.linkWriteUs': 'Write to us',
    'app.linkSiteMap': 'Site map',
    'app.labelSocialPanel': 'Recommend:',
    'app.labelOwner': 'ООО &laquo;Dressformer&raquo;',
    'app.tweet': 'Tweet',
    'auth.form.title': 'Authentication',
    'auth.form.description': 'In order to gain access to the tools of business sign in to your personal account using the username and password specified at registration',
    'auth.form.labelLogin': 'Login(e-mail)',
    'auth.form.plchLogin': 'Enter email',
    'auth.form.labelPassword': 'Password',
    'auth.form.plchPassword': 'Password',
    'auth.form.labelRememberMe': 'Remember me',
    'auth.form.linkForgotPassword': 'Forget password?',
    'auth.form.linkRegister': 'Register',
    'auth.form.btnSingIn': 'Sing in',
    // errors
    'auth.validate.emptyLogin': 'Login incorrect!',
    'auth.validate.emptyPassword': 'Password incorrect!',
    'auth.user.not.found': 'User not found!',
    // register
    'register.form.title': 'User registration',
    'register.form.labelLinkToAuth': 'Already a member?',
    'register.form.linkToAuth': 'Authenticate',
    'register.form.plchPasswordConfirm': 'Password confirm',
    'register.form.btnRegister': 'Registration',
    'register.form.labelSubscribe': 'Subscribe to updates of business tools of Dressformer',
    'register.form.labelConfirmTerms': 'I agree to the ',
    'register.form.termsAgreement': 'Terms of this agreement',
    'register.terms.cancel': 'Cancel',
    'register.terms.agree': 'Agree',
    'register.terms.title': 'Terms of the User Agreement',
    'register.terms.description': 'To use the system, you must accept the terms of the below User Agreement',
    'register.terms.notSupport': 'Your browser does not support the display of the document',

    'register.validate.notEqualPassw': 'The passwords do not match',
    'register.validate.emptyCaptcha': 'Captcha value is empty',

    // panel/setting
    'panel.settings.leftside.settings.title': 'Настройки',
    'panel.settings.leftside.settings.partner_profile': 'Профиль партнера',
    'panel.settings.leftside.settings.account': 'Аккаунт',
    'panel.settings.alert.for.leftside': '<strong>Вы используете демонстрационную версию каталога Dressformer для бизнеса.</strong>' +
        '<p>В этом режиме вы можете использовать весь функционал Каталога, но при этом вам не будет доступна функция публичного ' +
        'размещения лотов</p><br/>' +
        '<p>Для того чтобы разблокировать все инструменты Dressformer для бизнеса, вам необходимо заключить Договор ' +
        'сотрудничества, заполнив заявку на подключение к системе ниже.</p>'
};