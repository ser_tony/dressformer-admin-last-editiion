
window.App.setupForTesting();

window.App.injectTestHelpers();

module("Integration Tests", {
    setup: function() {
        Ember.run(App, App.advanceReadiness);
    },
    teardown: function(){
        App.reset();
    }
});
function sleep(ms) {
    ms += new Date().getTime();
    while (new Date() < ms){}
}
test("index redirect to auth and authenticate", function(){
    visit("/")
        .then(function() {
            sleep(400);
            return equal(window.App.get('currentPath'), 'auth', "The first page is auth");
        })
        .then(function(){
            sleep(400);
            find('#authInputLogin').val('').trigger('change');
            find('#authInputPassword').val('1').trigger('change');
            return click("#btnSingIn");
        })
        .then(function(){
            sleep(400);
            return ok(find("div.help-block:contains('Login incorrect!')").length, "Empty login");
        })
        .then(function(){
            sleep(400);
            find('#authInputLogin').val('12').trigger('change');
            find('#authInputPassword').val('').trigger('change');
            return click("#btnSingIn");
        })
        .then(function(){
            sleep(400);
            return ok(find("div.help-block:contains('Password incorrect!')").length, "Empty password");
        })
        .then(function(){
            sleep(400);
            find('#authInputLogin').val('tete').trigger('change');
            find('#authInputPassword').val('tete').trigger('change');
            return click("#btnSingIn");
        })
        .then(function(){
            sleep(400);
            return ok(find("div.help-block:contains('ERROR: User not found')").length, "User not found");
        })
        .then(function(){
            sleep(400);
            find('#authInputLogin').val('test').trigger('change');
            find('#authInputPassword').val('1').trigger('change');
            return click("#btnSingIn");
        })
        .then(function(){
            sleep(400);
            equal(window.App.get('currentPath'), 'panel', "Authenticate successful, redirect to panel");
        });
});

test("Check change i18n in Auth model", function(){
    visit("/")
        .then(function(){
            sleep(400);
            return ok(find(".legend_desc span:contains('In order to gain access to the tools of business sign in to your personal account using the username and password specified at registration')").length, "Default English, check this");
        })
        .then(function(){
            sleep(400);
            return click('.language button');
        })
        .then(function(){
            sleep(400);
            return ok(find('.language .dropdown-menu:visible').length, 'Show dropdown menu - list languages');
        })
        .then(function(){
            sleep(400);
            return click('.language .dropdown-menu:visible li:first a');
        })
        .then(function(){
            sleep(400);
            ok(find('label[for="authInputLogin"] span:contains("Логин(e-mail)")').length, 'Language changed to russian');
        })
});

test("Check Register Form", function(){
    visit("/register")
        .then(function(){
            sleep(400);
            return ok(find("#btnRegister").length, "Open page registration");
        })
        .then(function(){
            sleep(400);
            var linkModal = find('a[href="#TermsModal"]');
            return click(linkModal);
        })
        .then(function(){
            sleep(400);
            var modal = find('.modal-backdrop').is(':visible');
            return ok(modal, 'Show modal window - User Agreement');
        })
        .then(function(){
            sleep(400);
            var button = find('#TermsModal button')[1];
            return click(button);
        })
        .then(function(){
            sleep(400);
            ok(find('.confirmTerms').is(':checked'), 'Check User Agreement');
            ok(!find('#btnRegister').hasClass('disabled'), 'Button registration is not disabled');
        })
        .then(function(){
            sleep(400);
            var button = find('#TermsModal button')[0];
            return click(button);
        })
        .then(function(){
            sleep(400);
            ok(!find('.confirmTerms').is(':checked'), 'Uncheck User Agreement');
            ok(find('#btnRegister').hasClass('disabled'), 'Button registration is disabled');
        })
        .click(find('a[href="/auth"]'))
        .then(function(){
            sleep(400);
            return equal(window.App.get('currentPath'), 'auth', "Transition from register to auth form");
        })
});
