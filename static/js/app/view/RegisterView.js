App.RegisterView = Ember.View.extend({
    initedScroll: false,
    didInsertElement: function(){
        var me = this,
            modal = $('#TermsModal');

        //инициализация блока с CAPTCHA
        this.controller.initCAPTCHA();

        modal.on('shown.bs.modal', function(){
            if(!me.get('initedScroll')){
                $.get('https://legal.dressformer.com/fb_terms.html').done(function(response){
                    var a = response.match('<body(.*)?>((.*[\r\n]*)*)?</body>');
                    modal.find('.scrollContent').html(a[2]);
                    me.set('initedScroll', true);
                    modal.find(".nano").nanoScroller({
                        contentClass: 'scrollContent',
                        preventPageScrolling: true
                    });
                    
                });
            }
        });
    }
});