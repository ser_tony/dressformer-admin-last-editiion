App.SocialPanelView = Ember.View.extend({
    templateName: 'socialPanel',
    langBinding: Ember.Binding.oneWay('App.ApplicationController.lang'),
    fbAppId: '472262406181470',
    vkAppId: '3275783',

    notifyPropertyChange: function(key){
        var me = this;
        console.log('CHANGE ', key);
        if( key=='lang' ){
            me.rerender();
        }
    },
    /**
     * Вьюшка была вставлена в страницу.
     * Инициализируем виджеты!
     */
    didInsertElement: function(){
        var me = this,
            lang = Ember.util.getLanguage();

        window.___gcfg = {lang: lang.split('_')[0]};
        if(window.initSocials) {
            $('#google-script, #facebook-jssdk, #twitter-wjs').remove();
        }

        // Google plus
        (function () {
            var po = document.createElement('script');
            po.id = 'google-script';
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();
        // Facebook like
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "http://connect.facebook.net/" + lang + "/all.js#xfbml=1&appId="+ me.fbAppId;
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        if(window.FB) FB.XFBML.parse();

        // Twitter
        !function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + '://platform.twitter.com/widgets.js';
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, 'script', 'twitter-wjs');

        // VK like
        VK.Widgets.Like("vk_like", {type: "mini"});
        window.initSocials = true;
    }
});