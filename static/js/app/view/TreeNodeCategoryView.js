App.TreeNodeCategoryView = Ember.View.extend({
    template: Ember.Handlebars.compile('{{yield}}'),

     //отображать дочерние категории?
    isChildrenVisibility: function(){
        var me = this,
            openExist = me.get('controller').get('isOpenExisting');

        //TODO проверка isOpenExisting в контроллере
        var r = Em.get(me.templateData.keywords, 'category_model').get('children') && openExist ?
                'display:visible':'display:none';

        return r;
    }.property(),

    //отображать узлы с одеждой?
    isGarmentVisibility: function(){
        var me = this,
            openExist = me.get('controller').get('isOpenExisting');
        var r = Em.get(me.templateData.keywords, 'sub_category').get('children') && openExist ?
                'display:visible':'display:none';
        return r;
    }.property()

})

