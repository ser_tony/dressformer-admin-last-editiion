App.BlockPanelView = Em.View.extend({
    tagName: 'div',
    classNames: ['panel', 'panel-default'],
    init: function(){
        this._super();
        this.set('title', Em.I18n.translations[this.get('title')]);
    },
    layoutName: 'panel/block_panel'
});