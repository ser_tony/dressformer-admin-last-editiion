App.PanelCategoryContentView =  Ember.View.extend({
    tagName:'div',
    classNames: ['col-md-8', 'parent_lot_parameters', 'scroll'],
    template: Em.Handlebars.compile('{{yield}}'),
    didInsertElement: function(){
        this.$().jScroll();
    }
});