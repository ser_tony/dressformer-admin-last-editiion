App.ChartView = Em.View.extend({
    tagName: 'div',
    template: Em.Handlebars.compile(''),
    categories: [],
    data: [],
    optionsChart: {
        chart: {
            type: 'areaspline',
            backgroundColor: '#f9fafc',
            borderWidth: 0,
            plotBackgroundColor: '#f1f4f7',
            plotShadow: false,
            plotBorderWidth: 0
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: [],
            title: '',
            gridLineWidth: 1,
            gridLineColor: '#dadfe3',
            lineWidth: 2,
            lineColor: '#c4cbd6',
            tickWidth: 0,
            tickmarkPlacement: 'on'
        },
        yAxis: {
            title: '',
            minorTickInterval: 'auto',
            allowDecimals: false,
            lineWidth: 2,
            lineColor: '#c4cbd6',
            tickWidth: 0,
            min: 0
        },
        tooltip: {
            shared: true,
            valueSuffix: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        }
    },
    init: function(){
        var me = this;
        me._super();
    },
    didInsertElement: function(){
        var me = this;
        me.renderChart();
    },

    categoriesObserver: function(){
        var me = this;
        me.get('optionsChart').xAxis.categories = me.get('categories');
        me.renderChart();
    }.observes('categories'),

    renderChart: function(){
        var me = this;
        me.$().highcharts($.extend(me.get('optionsChart'), {
            series: [
                {
                    name: me.get('name'),
                    data: me.get('data')
                }
            ]
        }));
    }
});