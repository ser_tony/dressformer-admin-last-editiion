App.RegisterRoute = Ember.Route.extend({
    model: function(){
        return App.UserModel.create({});
    },

    setupController: function(controller, model){
        controller.set('model', model);
    }
});