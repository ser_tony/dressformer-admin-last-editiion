App.PanelCatalogRoute = Ember.Route.extend({

    //TODO route loading http://jschr.github.io/bootstrap-modal/

    init: function(){
        this._super();
        console.log('init PanelCatalogRoute');
    },
    activate: function(){
        var me = this;
        me.controllerFor('panel').set('activeMenu', 'catalog');
        console.log('PanelCatalogRoute active', me.controllerFor('panel').get('activeMenu'));
        me.controllerFor('panel.catalog').set('isClosedPanel', false);
        //инициализация контроллера дерава каталогов
//        me.controller.initCatalogTree();
    },

    setupController: function(controller, model) {
      controller.set('model', model);
    },

    renderTemplate: function(){
        //отрисовка родительского шаблона
        this.render('panel/catalog');

        //отрисовка дочерних outlet-ов
        this.render('panel/catalog/catalog_tree',{
            outlet: 'catalog_tree',
            into: 'panel/catalog',
            controller: 'panelCatalogCatalogTree'
        });

        this.render('panel/catalog/catalog_filter',{
          outlet: 'catalog_filter',
          into: 'panel/catalog',
          controller: 'panelCatalogCatalogFilter'
        });
    }
});