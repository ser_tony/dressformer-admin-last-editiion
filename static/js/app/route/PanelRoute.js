App.PanelRoute = Ember.Route.extend({

    isScrollInit: false,
    isAlertCloserInit: false,

    activate: function() {
        var me = this;

        me.initScrollPanel();
        me.initAlertCloser();

        //console.log('init PanelRoute');
        //if (!me.controllerFor('application').get('authenticated')) this.transitionTo('auth');
    },

    /**
     * инициализация закрытия алертов (сообщений пользователю)
     * */
    initAlertCloser: function(){
        var me = this;

        if (!me.isAlertCloserInit) {
            $(document).on('click', 'button.close', function(evt){
                var btn = $(evt.target),
                    parentAlert = btn.parents('.alert');
                parentAlert.remove();
                //TODO сохранение в куки флага - что пользователь уже видел данный алерт и целенаправленно закрыл

            });
            me.isAlertCloserInit = true;
        }
    },

    /** Инициализация смещения блока праметров лота при скроллинге*/
    initScrollPanel: function(){
        var me = this;
        if (me.isScrollInit) return;
        me.isScrollInit = true;


//        $(window).resize(function(){
//            var h =$('.panel.lot_parameters').height();
//            $('.panel.lot_parameters').parent().height(h+20).css('top', 0);
//        });

//
//        $().ready(function() {
//			$(".scroll").jScroll();
//		});

//        $(window).scroll(function(){
//            var panParent = $('.panel.lot_parameters').parent(),
//                top = panParent.offset().top - parseFloat(panParent.css('marginTop'));
//
//            var windowpos = $(window).scrollTop();
//            if (windowpos <top ){
//                panParent.css('position', 'static');
//            }
//            else {
//                panParent.css('position', 'fixed');
//                panParent.css('top', 0);
//            }
//        });


//
//        var tempScrollTop, currentScrollTop = 0;
//
//        $(window).scroll(function(){
//
//
//            //высота+ ширина окна браузера
//            var windowHeight = $(window).height(),
//                windowWidth = $(window).width(),
//                rowWrapperTop = $('.catalog_wrapper').offset().top,
//                rowWrapperH = $('.catalog_wrapper').height(),
//
//                //родитель блока панели
//
//                panParent = $('.panel.lot_parameters').parent();
//
//                currentScrollTop = $(window).scrollTop();
//
//                //move to up
////                if (tempScrollTop > currentScrollTop) {
////                    if (currentScrollTop > rowWrapperTop) {
////
////                    }
////                    else {
////                        panParent.css('top', 0);
////                    }
////                    return;
////                }
//
//
//            if ( windowWidth <=992) {
//                //возврат panel в исходное положение
//                //узнать текущее положение - на какое сдвинуть панель
//                //$('.catalog_wrapper').height(rowWrapperTop + rowWrapperH);
////                panParent.css('top',rowWrapperTop + rowWrapperH);
////                panParent.css('top', 0);
//                return
//            }
//            var
//                //блок параметров лота
//                panelLot = $('.panel.lot_parameters'),
//                wScrollTop = $(window).scrollTop(),
//                panelTop = panelLot.parent().offset().top,
//                panelH = panelLot.parent().height(),
//
//                //враппер ROW
//                wrapperR = $('.catalog_wrapper'),
//                wrapperH = wrapperR.height(),
//                rowWrapperTop = wrapperR.offset().top;
//
//            if (wScrollTop > rowWrapperTop) {
//                if ((panelTop + panelH) > (rowWrapperTop+ wrapperH))
//                {
//                //move to up
//                if (tempScrollTop > currentScrollTop) {
//                    if (currentScrollTop > rowWrapperTop) {
//                         var diffWindowWrapper = wScrollTop - rowWrapperTop;
//                        //смещение родителя panel
//                        panParent.css('top', diffWindowWrapper + 20);
//                    }
//                    else {
//                        panParent.css('top', 0);
//                        $('.panel.lot_parameters').css('top', 0);
//                    }
//
//                    tempScrollTop = currentScrollTop;
//                    return;
//                }
//                 else return;
//                }
//
//                var diffWindowWrapper = wScrollTop - rowWrapperTop;
//                //смещение родителя panel
//                panParent.css('top', diffWindowWrapper + 20);
//
//            }
//            else {
//                //возврат panel в исходное положение
//                panParent.css('top', 0);
//                $('.panel.lot_parameters').css('top', 0);
//            }
//
//            tempScrollTop = currentScrollTop;
//
////
////
////            if ( wScrollTop >= panelTop) {
////                //привязка панели к низу страницы
////
////                //разница в высоте окна экрана и блока-панели
////                var diffHeight = windowHeight - panelLot.height(),
////
////                    //реальная высота контента страницы
////                    contentHeight = $(document).height();
////
////
////                //высота окна больше высоты панели
////                if (diffHeight>0){
////                    //var x = wScrollTop + diffHeight;
////                     panelLot.css('top',diffHeight);
////                }
////                else {
////
////                }
////
////            }
////            else {
////                panelLot.css('top',0);
////            }
//        });


    }

});