App.PanelCatalogGarmentNewRoute = Ember.Route.extend({

    model: function(){
        var me = this;
        var parentCategory = me.controllerFor('panel.catalog.catalog.tree').get('tempParentCategory');
        var model = App.GarmentModel.create({
            category: parentCategory,
            color: App.ColorModel.create({name: '-'}),
            shop: App.ShopModel.create({title: '-'}),
            brand: App.BrandModel.create({name: '-'}),
            isError: true
        });
        debugger
        return model
    },

    setupController: function(controller, model){
        if(!model) return;
        model.set('defaultModel', model.clone());
        this.controllerFor('panel.catalog.garment')
            .set('model', model);

        debugger
    }
});