App.PanelSettingsPartnerRoute = Ember.Route.extend({

    model: function(){
        return App.User;
    },

    init: function(){
        this._super();
        console.log('PanelSettingsPartnerRoute INIT');
    },
    activate: function(){
        var me = this;
        me.controllerFor('panel').set('activeMenu', 'settings');
        me.transitionTo('panel.settings.partner.individual');
    }
});