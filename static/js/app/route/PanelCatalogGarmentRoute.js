App.PanelCatalogGarmentRoute =  Ember.Route.extend({
    tagBtnInit: false,
    model: function(p, t){
        var me = this,
            parentCategory,
            m = t.providedModels['panel.catalog.garment.edit'];
        if(m) {
            parentCategory = m.get('category');
        } else parentCategory = me.controllerFor('panel.catalog.catalog.tree').get('tempParentCategory');
        var model = App.GarmentModel.create({
            category: parentCategory,
            color: App.ColorModel.create({name: '-'}),
            shop: App.ShopModel.create({title: '-'}),
            brand: App.BrandModel.create({name: '-'}),
            isError: true
        });
        return model;
    },

    setupController: function(controller, model){

        controller.set('model', model);
        controller.set('defaultModel', model.clone());
        controller.get('controllers.panelCatalog').closeEmptyPanel();
        controller.resetProperties();
    },

    activate: function(){
        var me = this;
        if(!me.tagBtnInit){
            me.tagBtnInit =true;
            $(document).on('click', '.tag_btn', function(ev){
                var bt = $(ev.target);
                bt.toggleClass('active');
            });
        }
    }
});