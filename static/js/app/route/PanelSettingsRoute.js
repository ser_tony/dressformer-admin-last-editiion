App.PanelSettingsRoute = Ember.Route.extend({

    init: function(){
        this._super();
        console.log('PanelSettingsRoute INIT');
    },
    activate: function(){
        var me = this;
        me.controllerFor('panel').set('activeMenu', 'settings');
        me.transitionTo('panel.settings.partner.individual');
    }
});