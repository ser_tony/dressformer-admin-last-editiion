App.PanelCatalogGarmentEditRoute = Ember.Route.extend({
    serialize: function(m){
        return {id: m.get('id')};
    },

    model: function(params){
        App.GarmentModel.create({}).find({id: params.id}).then(function(m){
            this.controllerFor('panel.catalog.garment')
            .set('model', m);
        });
        return;
    },

    setupController: function(controller, model){
        if(!model) return;
        console.log(model);
        this.controllerFor('panel.catalog.garment')
            .set('model', model.clone());
        this.controllerFor('panel.catalog.garment')
            .set('realModel', model);
    }
});