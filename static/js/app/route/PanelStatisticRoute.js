App.PanelStatisticRoute = Ember.Route.extend({
    activate: function(){
        var me = this;
        me.controllerFor('panel').set('activeMenu', 'statistic');
        me.transitionTo('panel.statistic.content');
    }
});