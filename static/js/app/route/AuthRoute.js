App.AuthRoute = Ember.Route.extend({

    model: function(){
        App.User = App.User || App.UserModel.create({});
        return App.User;
    },

    setupController: function(controller, model){
        controller.set('model', model);
    }
});