App.PanelStatisticLoaderRoute = Ember.Route.extend({

    model: function(){
        var me = this,
            category = App.CategoryModel.create({}),
            from = me.controllerFor('panelStatistic').get('dateFrom'),
            to = me.controllerFor('panelStatistic').get('dateTo'),
            model = App.GarmentModel.create({});
        return new Ember.RSVP.Promise(function(resolve, reject) {
            var promise = new Ember.RSVP.Promise(function(resolve2, reject2) {
                if(me.controllerFor('panelStatistic').get('selected_category').id == -1){
                    category.find().then(function(cats){
                        me.controllerFor('panelStatistic').set('categories', cats);
                        me.controllerFor('panelStatistic').set('selected_category', cats[0]);
                        resolve2(cats[0]);
                    }, function(){
                        reject2(arguments);
                    });
                } else {
                    resolve2(me.controllerFor('panelStatistic').get('selected_category'));
                }
            });
            promise.then(function(category){
                model.find({
                    api: 'report_audit_total_list_all',
                    data: {
                        categories: category.get('id'),
                        from: from,
                        to: to
                    }
                }).then(function(garments){
                    resolve(garments);
                }, function(){
                    reject(arguments);
                });
            }, function(){
                reject(arguments);
            });

        });
    },

    setupController: function(controller, model){
        controller.set('allGarments', model);
        controller.setAllModel();
    }
});