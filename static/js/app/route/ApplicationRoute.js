App.ApplicationRoute = Ember.Route.extend({

    actions:{
        loadCategories: function(params){
            var container = params[0],
                model = params[1];
            if(!model){
                model = App.CategoryModel.create({});
                model.find().then(function(models){
                    container.pushObjects(models);
                });
            } else {
                model.find({data: {parents: model.get('id')} }).then(function(child_data){
                    if(typeof model.get('children').length == 'undefined') model.set('children', []);
                    model.get('children').pushObjects(child_data);
                });
            }
        }
    }
});