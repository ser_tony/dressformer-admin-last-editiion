App.PanelStatisticViewRoute = Ember.Route.extend({

    serialize: function(m){
        return {id: m.get('id')};
    },

    model: function(params){
        var me = this,
            parent_controller = me.controllerFor('panelStatistic'),
            garment = App.GarmentModel.create({}),
            result = me.controllerFor('panelStatisticContent') && me.controllerFor('panelStatisticContent').get('allGarments') && me.controllerFor('panelStatisticContent').get('allGarments').findBy('id', parseInt(params.id, 10));
        return new Ember.RSVP.Promise(function(resolve, reject) {
            var promise = new Ember.RSVP.Promise(function(resolve2, reject2) {
                if(result){
                    resolve2(result);
                } else {
                    garment.find({'api': 'garment_list_all', data:{ids: parseInt(params.id, 10)}}).then(function(lgarment){
                        var result = lgarment[0];
                        resolve2(result);
                    }, function(){reject2(arguments);});
                }
            });
            promise.then(function(result){
                garment = App.GarmentModel.create({});
                garment.find({
                    api: 'report_audit_daily_total_list_all',
                    data:{
                        garments: parseInt(params.id, 10),
                        from: parent_controller.get('dateFrom'),
                        to: parent_controller.get('dateTo'),
                        data: 'fitting,redirect,wardrobe'
                    }
                }).then(function(m){
                    result.set('statistic', m);
                    resolve(result)
                }, function(){reject(arguments)});
            }, function(){reject(arguments)});

        });
    },

    setupController: function(c, m){
        c.changedFilter();
        c.set('model', m);
        c.drawGraphics(1,1,1);

    }
});