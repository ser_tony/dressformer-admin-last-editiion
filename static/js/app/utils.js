/* Init methods over prototypes */
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

/* Configure jQuery */
jQuery.support.cors = true;

/* Ember.utils */
Ember.util = {
    /**
     * Поддерживаемые языки интерфейса
     */
    languages: ['en_US', 'ru_RU'],

    /**
     * Получение значения cookie по имени.
     * @param name - имя cookie
     * @returns {string} - значение установленной cookie
     */
    getCookie: function (name) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? (decodeURIComponent ? decodeURIComponent(matches[1]) : matches[1]) : undefined
    },

    /**
     * Установка значения cookie.
     */
    setCookie: function (name, value, expires, path, domain, secure) {
        expires instanceof Date ? expires = expires.toGMTString() : typeof(expires) == 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
        var r = [name + "=" + escape(value)], s, i;
        for (i in s = {expires: expires, path: path, domain: domain}) {
            s[i] && r.push(i + "=" + s[i]);
        }
        return secure && r.push("secure"), document.cookie = r.join(";"), true;
    },

    /**
     * Меняет значение языка интерфейса, перегружает страницу.
     * @param lang Имя языка интерфейса
     */
    changeLaguage: function (lang) {
        if (!this.languages.find(function (item) {
            return lang == item;
        })) lang = 'ru_RU';
        this.setCookie('lang', lang);
        document.location.reload();
    },

    /**
     * Достает из cookies значение установленного языка,
     * если не установлено то берет по умолчанию en_US.
     * @returns {string} Имя языка интерфейса.
     */
    getLanguage: function () {
        var lang = this.getCookie('lang') || 'ru_RU';
        if (!this.languages.find(function (item) {
            return lang == item;
        })) lang = 'ru_RU';
        return lang
    },

    /** Инициализация локальной БД
     * @return {Boolean}
     */
    initDB: function () {
        var me = this,
            current_version = 2;
        if (!(window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || false)) {
            if ('localStorage' in window && window['localStorage'] !== null) {
                me.dbType = 'localStorage';
                me.db = window['localStorage'];
                if (me.db.version != current_version) {

                    me.db.version = current_version;
                    me.db.setItem('TEMPLATES', '{}');
                }
            } else {
                me.dbType = 'dom';
                me.db = {
                    TEMPLATES: {}
                };
            }
            return;
        }
        me.dbType = 'indexedDB';
        me.db = window.linq2indexedDB("Dressformer", {
            version: current_version,
            definition: [
                {
                    version: current_version,
                    onupgradeneeded: function (transaction, oldVersion, newVersion) {
                    },

                    objectStores: [
                        {
                            name: 'TEMPLATES',
                            objectStoreOptions: {
                                autoIncrement: false,
                                keyPath: 'name'
                            }
                        }
                    ],
                    indexes: [
                        {
                            objectStoreName: 'TEMPLATES',
                            propertyName: 'name',
                            indexOptions: {
                                unique: true
                            }
                        }
                    ]
                }
            ]
        }, true);
        return true;
    },

    /**
     * Утилиты для работы с локальной базой
     */
    dbUtil: {
        /**
         * Вставить в БД загруженный шаблон
         * @param name - имя шаблона
         * @param tpl - текст шаблона
         */
        insertTpl: function (name, tpl) {
            var me = this,
                db = Ember.util.db;
            if (!(window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || false)) {
                if ('localStorage' in window && window['localStorage'] !== null) {
                    var musics = jQuery.parseJSON(db.getItem('TEMPLATES'));
                    musics[name] = tpl;
                    db.setItem('TEMPLATES', JSON.stringify(musics));
                } else {
                    db['TEMPLATES'][name] = tpl;
                }
            } else {
//                db.linq.from("TEMPLATES").insert({name: name, template: tpl}).done(function(o){
//                    console.log('template ' + name + ' insert into db successful');
//                });
            }
        },

        /**
         * Асинхронная загрузка шаблона с сервера
         * @param name - имя шаблона
         * @param callback
         */
        loadTpl: function (name, callback) {
            var me = this,
                url = '/static/js/app/tpl/',
                path_segments = name.split('.');
            url += path_segments.join('/') + '.hbs';
            $.get(url).done(function (response) {
                callback(response);
                me.insertTpl(name, response);
            });
        },

        /**
         * Достает шаблон из локальной базу, если такого нет то загружает, добавляет и возвращает
         * @param name - имяшаблона
         * @param callback
         * @returns {*|deferred.promise|Ember.PromiseProxyMixin.promise|Transition.promise}
         */
        getTpl: function (name, callback) {
            var me = this,
                db = Ember.util.db,
                defer = $.Deferred();
            if (!(window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || false)) {
                if ('localStorage' in window && window['localStorage'] !== null) {
                    var musics = jQuery.parseJSON(db.getItem('TEMPLATES'));
                    if (musics[name]) {
                        defer.resolve(musics[name]);
                    } else {
                        me.loadTpl(name, defer.resolve);
                    }
                } else {
                    defer.resolve(db['TEMPLATES'][name]);
                }
            } else {

                db.linq.from('TEMPLATES').where("name").equals(name).select().done(function (results) {
                    if (results && results.length) {
                        defer.resolve(results[0].template);
                    } else {
                        me.loadTpl(name, defer.resolve);
                    }
                });
            }
            return defer.promise();
        }
    },

    delIndexedDB: function () {
        Ember.util.db.deleteDatabase();
    }

};


/* reopen Ember classes */
(function () {

    //радио баттон
    Ember.RadioButton = Ember.View.extend({
        tagName: "input",
        type: "radio",
        attributeBindings: [ "name", "type", "value", "checked:checked:" ],
        click: function () {
            this.set("selection", this.$().val())
        },
        checked: function () {
            return this.get("value") == this.get("selection");
        }.property()
    });

    /******* Handlebars Helpers **********/
    // Helper for compare two values. Using: {{#compare value 12 operator=">"}}ok{{else}}no{{/compare}}
    // Notice: dynamic value from model use only in first argument(lvalue or "value" in example ^^)
    Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {
        if (arguments.length < 3)
            throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

        var operator = options.hash.operator || "==";
        lvalue = Ember.get(this, lvalue);
        if(options.hash.is_values){
            rvalue = Ember.get(this, rvalue);
        }
        var operators = {
            '==':       function(l,r) { return l == r; },
            '===':      function(l,r) { return l === r; },
            '!=':       function(l,r) { return l != r; },
            '<':        function(l,r) { return l < r; },
            '>':        function(l,r) { return l > r; },
            '<=':       function(l,r) { return l <= r; },
            '>=':       function(l,r) { return l >= r; },
            'typeof':   function(l,r) { return typeof l == r; }
        }

        if (!operators[operator])
            throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);

        var result = operators[operator](lvalue,rvalue);

        if( result ) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
    // Helpers for components
    Ember.Handlebars.helper('labelForSelectField', function(model, labelAttr, parentAttr, options) {
        var devider = (typeof options != 'undefined' && options.hash.devider) || (parentAttr.hash && parentAttr.hash.devider) || ' - ';
        var depth = (options && options.hash && options.hash.depth && parseInt(options.hash.depth, 10)) || (parentAttr.hash && parentAttr.hash.depth && parseInt(parentAttr.hash.depth, 10)) || 0;
        var prev_space = '';
        for(var i=0; i<depth; i++){
            prev_space += '-';
        }
        if(typeof model == 'string'){
            model = Ember.get(this, model);
            labelAttr = Ember.get(this, labelAttr);
        }
        var result = (!parentAttr && prev_space || '') + model[labelAttr];
        return (parentAttr && model['parent'] && model['parent']['name'] + devider + result) || result;
    });
    // Using: {{labelColor model "color.color"}}
    Ember.Handlebars.helper('labelColor', function(model, colorAttr) {
        if(typeof model == 'string'){
            model = Ember.get(this, model);
            colorAttr = Ember.get(this, colorAttr);
        }
        if(!colorAttr) return '';
        var color_path = colorAttr.split('.');
        for(var i=0; i<color_path.length; i++){
            model = model[color_path[i]];
        }
        return new Handlebars.SafeString('<div class="color_lot" style="background-color: #' + model + '"></div>');
    });
})();