App.SelectFieldComponent = Em.Component.extend({
    attributeBindings: ['selected'],
    idElement: '',
    // Определяет будет ли блок расширяться по всей допустимой длине, или будет как кнопка, по ширине текста
    isBlock: true,
    // То что будет на кнопке если не установлен defaultValue
    emptyText: 'Не выбрано',
    // Размер кнопки
    size: 'btn-sm',
    // Текущий выбранный элемент
    selected: null,
    // Имя атрибута цвета, если задан то будет отображаться квадратик данного цвета
    colorAttribute: null,
    // isTree - определяет дерево это или просто список
    isTree: false,
    // если True то при раскрытии списка не будет делать запрос, а будет сразу отрисовывать то что в model. Это например для месяцев
    isStatic: false,
    // именя id и label аттрибутов
    idAttribute: 'id',
    labelAttribute: 'name',
    // Модель, содержит массив данных
    model: [],
    // Значение по умолчанию в том же формате что и данные, например {id:-1, name:'Все'},
    // если null то берется первое значение из данных в model. Если model пустой, то выводит emptyText и значение -1
    defaultValue: null,
    // глубина дерева
    depth: 2,

    // КОНЕЦ параметров!

    init: function(){
        var me = this;
        me._super();
        if(me.get('selected')) return;
        if(!me.get('defaultValue')){
            if(me.get('model').length > 0){
                me.set('selected', me.get('model')[0]);
            } else {
                var selected = {};
                selected[me.get('idAttribute')] = -1;
                selected[me.get('labelAttribute')] = me.get('emptyText');
                me.set('selected', selected);
            }
        } else {
            me.set('selected', me.get('defaultValue'));
        }
    },

    // Вспомогательные функции, НЕ ТРОГАТЬ!
    fullWidth: function(){
        return this.get('isBlock') && 'width: 100%;' || '';
    }.property('isBlock'),

    actions: {
        triggerLoad: function(selected, isLast){
            var me = this;
            if(!selected && !me.get('isStatic')){
                me.set('model', []);
                me.sendAction('actionLoad', [me.get('model')]);
                return
            }
            if(selected){
                if(isLast || !me.get('isTree') || me.get('isStatic')){
                    me.set('selected', selected.clone && selected.clone() || Em.copy(selected));
                    me.sendAction('actionSelected', [me.get('selected'), me.get('idElement')]);
                } else {
                    if(selected.get('is_collapsed')){
                        selected.get('children').clear();
                        selected.set('is_collapsed', false);
                        return;
                    }
                    me.sendAction('actionLoad', [me.get('model'), selected]);
                    selected.set('is_collapsed', true);
                }
            }
        },

        triggerSelect: function(model){
            var me = this;
            me.set('selected', model.clone && model.clone() || Em.copy(model));
            me.sendAction('actionSelected', [me.get('selected'), me.get('idElement')]);
        }
    }
});