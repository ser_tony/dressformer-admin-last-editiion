App.ColorModel = App.BaseModel.extend({
    fields: ['id', 'name', 'color'],
    api_name: {
        get: 'color-list-all'
    }
});