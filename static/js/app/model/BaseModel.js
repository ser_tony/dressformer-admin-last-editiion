App.BaseModel = Ember.Object.extend({
    realm: 'api.dressformer.com',
    api_schema: 'http',
    api_space: 'api/',
    test: true,

    isChange: false,

    /*
     api_name: {
     get: 'qqqq',
     create: 'ssss',
     update: 'eeee',
     delete: 'rrr'
     }
     */
    api_name: false,
    idAttribute: 'id',
    fields: [],
    secure_api: ['security-authorise', 'user-create', 'security-get-captcha-public-key', 'security-renew-secret'],

    getUrl: function (method, api) {
        var me = this;
        if (api) {
            return me.get('api_schema') + '://' + ( me.get('test') ? 'test.' : '' ) + me.get('realm') + '/' + me.get('api_space') + me.get('api_name')[method][api];
        }
        return me.get('api_schema') + '://' + ( me.get('test') ? 'test.' : '' ) + me.get('realm') + '/' + me.get('api_space') + me.get('api_name')[method];
    },

    init: function () {
        var me = this;
        if (!me.get('api_name')) {
            throw new Error("Не указано имя метода API");
            return;
        }
        me.EVENTS = {};
        var props = this.get('fields');
        Ember.assert("fields should be an array", Ember.isArray(props));
        props.forEach(function (property) {
            // invoke <property>Changed when <property> changes ...
            Ember.addObserver(this, property, this, '_triggerChangeField');
        }, this);
        //me.properties = [me.idAttribute];
    },

    _triggerChangeField: function () {
        this.set('isChange', true);
    },

    /**
     * Метод для поиска, получения данных с сервера.
     * Данный метод использует матод API описанный в Model.api_name.get
     * Если передано params.api - тогда метод API будет соответствовать Model.api_name.get[params.api]
     *
     * @param params - объект параметров запроса. params.data - Данные которые будут оптравленны GET запросом на сервер.
     * @returns {*}
     */
    find: function (params) {
        var me = this,
            goodParams = (params && params.api ? params : false) || {'api': false},
            url = me.getUrl('get', goodParams.api),
            result = {};
        params = $.extend({}, params);

        if (!params.data) {
            params.data = {};
            if (me.get(me.idAttribute)) {
                params.data[me.idAttribute] = me.get(me.idAttribute);
            }
        }


        return new Ember.RSVP.Promise(function (resolve, reject) {
            me._request(url, params.data, 'GET').then(function (result) {

                if (result.status) {
                    reject(result)
                } else {
                    var data;
                    if (Array.isArray(result.result)) {
                        data = []
                        result.result.forEach(function (el) {
                            data.push(me.constructor.create(el));
                        });
                    } else {
                        data = me.constructor.create(result.result);
                    }
                    resolve(data);
                }
            }, function (error_response) {
                reject(error_response);
            });
        });
    },

    /**
     * Создание/редактирование объекта (асинхронное)
     */
    save: function (params, stict) {
        var me = this,
            data = me.getModelValues(),
            url = '';
        params = params || {};
        if(stict){
            data = params;
        } else {
            if (params) {
                me.get('fields').forEach(function(key){
                    if (typeof params[key] == 'undefined') return;
                    data[key] = params[key];
                });
            }
        }
        var method = 'create';
        if (me.get(me.idAttribute)) {
            method = 'update';
        }
        url = me.getUrl(method);
        var getData = (function(data){
            return data;
        })(data);

        return new Ember.RSVP.Promise(function (resolve, reject) {


            me._request(url, getData, 'POST').then(function (result) {

                if (result.status) {
                    //внутренняя ошибка результата запроса
                    //опредяется по статусу
                    reject(result);
                } else if (result.result) {
                    me.setModelValues(data);
                    me.setModelValues(result.result);
                    resolve(me);
                } else {
                    reject(result);
                }
            }, function (error_result) {

                //ошибка на уровне связи с сервером API
                reject(error_result);
            });
        });
    },

    /**
     * Удаление объекта (асинхронное)
     *
     * @param api - ключ удаления в списке API данной модели
     * @param not_remove - если true объект не будет удален с сервера
     *
     * */
    destroy: function (not_remove) {
        var me = this,
            result = {};

        return new Ember.RSVP.Promise(function (resolve, reject) {
            if (not_remove) {
                me._super();
                resolve();
                return;
            }
            if (me.get(me.idAttribute)) {
                    var url = me.getUrl('destroy');
                    var data = {};
                    data[me.idAttribute] = me.get(me.idAttribute);
                    //удалить с сервера
                    me._request(url, data).then(function (result) {
                        if (result.status) {
                            //внутренняя ошибка обработки ответа
                            reject(result);
                        }
                        else {
                            me._super();
                            resolve(result);
                        }
                    }, function (error_result) {
                        //ошибка на уровне связи с сервером API
                        reject(error_result);
                    });
            } else {
                reject();
            }
        });
    },

    _signature: function (api_name) {
        var me = this,
            time = new Date().getTime();
        //Текст подписи
        var signatureSource = App.User.get('login') + ":" + me.realm + ":" + me.api_space + api_name + ":" + time;
        //Шифруем подпись
        var signature = CryptoJS.HmacSHA256(signatureSource, App.User.get('secret'));
        //Подготавливаем полный заголовок запроса
        var authorisation = "signed " + App.User.get('login') + ":" + me.realm + ":" + me.api_space + api_name + ":"
            + time + ":" + App.User.get('timeout') + ":" + signature;
        //Переводим заголовок в Base64
        return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(authorisation));
    },

    _request: function (url, data, method) {
        var me = this,
            result = {},
            headerParams = {};
        method = method ? method : 'POST';

        var paths = url.split('/');
        var api_name = paths.pop();
        if (!me.secure_api.contains(api_name)) {
            headerParams = {'authorization': me._signature(api_name)}
        }


        return new Ember.RSVP.Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                method: method,
                dataType: "json",
                beforeSend: function (request) {
                    if (!me.secure_api.contains(api_name)) {
                        request.setRequestHeader("authorization", headerParams.authorization);
                    }
                },
                headers: headerParams,
                async: true,
                data: data,
                success: function (response) {
                    console.log('RESULT', response);
                    resolve(response);
                },
                error: function (req, status, error) {
                    console.log("Error request:", error.message);
                    result = {
                        status: 1,
                        message: error.message
                    }
                    reject(result);
                }
            });
        });
    },

    getModelValues: function () {
        var me = this,
            result = {};
        for (var i = 0; i < me.get('fields').length; i++) {
            var key = me.get('fields')[i];
            result[key] = me.get(key);
        }
        return result;
    },

    setModelValues: function (obj, replace) {
        var me = this;
        for (var i = 0; i < me.get('fields').length; i++) {
            var key = me.get('fields')[i];
            if (typeof obj[key] == 'undefined' && !replace) {
                continue;
            }
            me.set(key, obj[key]);
        }
    },

    clone: function () {
        var me = this;
        return me.constructor.create(me.getModelValues());
    }

});