App.ShopModel = App.BaseModel.extend({
    fields: ['id', 'title', 'url', 'logoUrl'],
    api_name:{
        get: 'shop-list-all'
    }
});