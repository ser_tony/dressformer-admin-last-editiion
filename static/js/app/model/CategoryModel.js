/**
 * Модель категорий лотов
 *
 *  атрибуты модели:
 *  id	идентификатор категории
    name	название
    parent	родительская категория. Может быть пустой
    slot	слот категории
    children	вложенные категории.
 */
App.CategoryModel = App.BaseModel.extend({
    fields: ['id', 'name', 'parent', 'slot', 'children', 'garments', 'test'],

    //открыта категория в дереве
    isOpen: false,

    //флаг - есть ли в данной категории лоты
    isNotLots: true,

    childCount: 0,

    //загрузка объекта
    loading: false,
    test: [],

    api_name: {
        get: 'category-list-all'
    },

    //есть ли дочерние категории в данный момент
    childrenExist: function(){
        var children =this.get('children');
        return (typeof children!='undefined' && children.length>0);
    }.property('children'),


    //есть ли вложенная одежда
    garmentsExist: function() {
        var garm = this.get('garments');
        return (typeof garm !='undefined' && garm.length>0);
    }.property('garments'),

    getChildren: function(){
        return this.get('children');
    }.property('children')

});