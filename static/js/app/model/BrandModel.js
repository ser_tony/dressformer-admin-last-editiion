App.BrandModel = App.BaseModel.extend({
    fields: ['id', 'name'],

    api_name: {
        get: 'brand-list-all'
    }

});