App.UserModel = App.BaseModel.extend({
    fields: [
        'id', 'login', 'email', 'password', 'avatarUrl', 'firstName', 'middleName',
        'lastName', 'birthDate', 'genderid', 'city', 'followerCount',
        'followingCount', 'followedByCurrent', 'recaptchaChallenge', 'recaptchaResponse',
        'secret', 'timeout'
    ],
    login: 'test',
    password: '1',
    'email': '',
    rememberMe: false,
    passwordConfirm: '',

    api_name: {
        get: {
            security_authorise: 'security-authorise',
            user_get_by_fb_id: 'user-get-by-fb-id',
            user_get_by_vk_id: 'user-get-by-vk-id',
            user_list_all: 'user-list-all',
            user_follower_list_all: 'user-follower-list-all',
            user_following_list_all: 'user-following-list-all',
            security_renew_secret: 'security-renew-secret'

        },
        create: 'user-create',
        update: {
            user_update_from_vk: 'user-update-from-vk',
            user_update_from_fb: 'user-update-from-fb'
        }
    },

    /**
     * Аутентификация пользователя в системе (асинхронный запрос)
     */
    authenticate: function () {
        var me = this,
            nonce = Math.floor(Math.random() * 10000),
            timestamp = new Date().getTime(),
            data = {
                login: me.get('login'),
                nonce: nonce,
                timestamp: timestamp.toString(),
                token: '' + CryptoJS.MD5(timestamp.toString() + nonce.toString() + me.get('password'))
            },
            url = me.getUrl('get', 'security_authorise');


        return new Ember.RSVP.Promise(function (resolve, reject) {
            me._request(url, data, 'GET').then(function (result) {
                if (result.status) {
                    //внутренняя ошибка авторизации
                    reject(result);
                } else {
                    me.set('secret', result.result.secret);
                    me.set('timeout', result.result.timeout);
                    // timeout - 3 minutes
                    me.set('runner_update_secret', Ember.run.later(function () {
                            me.updateSecret();
                        }, (new Date(me.get('timeout'))) - (new Date()) - (1000 * 60 * 3))
                    );
                    console.log('TIMEOUT', me.timeout, (new Date(me.timeout)).getTime(), timestamp, timestamp - (new Date(me.timeout)).getTime());
                    me.setModelValues(result.result.user);
                    me.set('password', '');
                    Ember.util.setCookie('user', CryptoJS.enc.Utf8.parse(JSON.stringify(me.getModelValues()))
                        .toString(CryptoJS.enc.Base64));
                    resolve(me);
                }
            }, function (error_result) {
                reject(error_result);
            });
        });

    },

    updateSecret: function () {
        return new Ember.RSVP.Promise(function (resolve, reject) {
            resolve();
        })
    },

    /**
     *
     * @returns {boolean} true - пользователь создан, false - в процессе создания ошибка
     */
    register: function () {
        return this.save();
    }


});