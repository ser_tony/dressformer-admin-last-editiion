/**
 * Модель Элемент одежды
    id	идентификатор одежды
    article	артикул
    internalId	Внутренний идентификатор
    url	ссылка на страницу в интернет-магазине
    previewUrl	полный url картинки с превью
    modelUrl	полный url файла модели
    modelSize	длина файла модели в байтах
    category	категория одежды Category без вложенных категорий

 * */

App.GarmentModel = App.BaseModel.extend({
    fields: ['id', 'article', 'title', 'internalId', 'url', 'brand', 'structure', 'shop', 'description',
        'getBGColor', 'previewUrl', 'modelUrl', 'modelSize', 'category', 'color', 'categoryId',
        'brandId', 'shopId', 'colorId', 'createdString', 'selected'
    ],

    selected: false,

    /** валидация модели
     *
     * @return - массив полей с ошибкой валидации либо пустой массив
     * */
    validate: function(){
        var me = this,
            notValidParams =[];

        if(!me.get('shop').id ) {
            notValidParams.push('shop');
        }
        if (!me.get('brand').id ) {
            notValidParams.push('brand');
        }

        if (!me.get('article')){
            notValidParams.push('article');
        }

        if (!me.get('color').id){
            notValidParams.push('color');
        }

        if (!me.get('category').parent) {
            notValidParams.push('category')
        }

//        if (!me.get('preview')) {
//            notValidParams.push('preview');
//        }
//
//        if (!me.get('model')) {
//            notValidParams.push('model');
//        }


        return notValidParams;
    },

    //ошибка в заполнении модели
    isError: false,

    //описание ошибки TODO computed property to know is error
    errorMsg: 'ошибки распознавания',

    //модель только созданная на клиенте
    isNew: false,

    api_name: {
        get: {
            garment_list_all: 'garment-list-all',
            garment_get_by_article_list_all: 'garment-get-by-article-list-all',
            report_audit_total_list_all: 'report-garment-audit-total-list-all',
            report_audit_daily_total_list_all: 'report-garment-audit-daily-total-list-all',
            report_garment_audit_pdf: 'report-garment-audit-pdf'
        },
        create: 'garment-save',
        update: 'garment-save'
    },

    getBGColor: function() {
        var colResult = '';
        if (this.get('color')) {
            colResult = 'background-color: #' + this.get('color').color;
        }
        return colResult;
    }.property('color'),

    createdString: function(){
        var now = new Date(),
            temp = new Date(this.get('created'));
        if(now.getFullYear() == temp.getFullYear() && now.getMonth() == temp.getMonth() && (now.getDate() - temp.getDate() <= 5 )){
            return moment(this.get('created')).startOf('day').fromNow();
        }
        return moment(this.get('created')).format('LL');
    }.property('created'),

    colorString: function(){
        return this.get('color') && 'background-color: #' + this.get('color') || '';
    }.property('color'),

    categoryString: function(){
        if( typeof this.get('category') == 'string'){
            return this.get('category');
        }
        return this.get('category').name;
    }.property('category'),

    brandString: function(){
        if( typeof this.get('brand') == 'string'){
            return this.get('brand');
        }
        return this.get('brand').name;
    }.property('brand')

});
