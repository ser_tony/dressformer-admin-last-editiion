App.PanelStatisticReportController = Ember.ArrayController.extend({
    needs: ['panelStatistic'],
    parent_controller: Ember.computed.alias("controllers.panelStatistic"),

    // все загруженные лоты по фильтру без пагинации
    allGarments: [],

    // сгенерированный url для скачивания статистики
    urlReport: function(){
        var me = this,
            model = App.GarmentModel.create({}),
            garmentsString = me.get('allGarments').filterBy('selected', true).getEach('id').join(','),
            url = model.getUrl('get', 'report_garment_audit_pdf') + '?',
            api_name = model.getUrl('get', 'report_garment_audit_pdf').split('/').pop(),
            authorization = model._signature(api_name);

        return url + $.param({
            from: me.get('parent_controller').get('dateFrom'),
            to: me.get('parent_controller').get('dateTo'),
            garments: garmentsString,
            article: me.get('articleQuery'),
            authorization: authorization
        });
    }.property('selected_period', 'articleQuery', '@each.selected'),

    selectedCount: function(){
        console.log('RECALCULATE');
        return this.get('allGarments').filterBy('selected', true).length;
    }.property('@each.selected'),

    totalCount: function(){
        return this.get('allGarments').length;
    }.property('allGarments'),

    isHtmlFormat: 0,
    isPdfFormat: 1,
    allChecked: false,

    // значения поля фильтра поиска по артиклу
    articleQuery: function(){
        return this.get('parent_controller').get('articleQuery');
    }.property('parent_controller.articleQuery'),

    // Выпадающий список категорий
    categories: [],
    selected_category: function(){
        return this.get('parent_controller').get('selected_category');
    }.property('parent_controller.selected_category'),

    // Пагинация
    pages: [{page: 1, active: true}],
    last_page: true,
    page: 0,
    limit: 10,

    // Выпадающий список периода
    selected_period: function(){
        return this.get('parent_controller').get('selected_period');
    }.property('parent_controller.selected_period'),
    periods: function(){
        return this.get('parent_controller').get('periods');
    }.property('parent_controller.periods'),

    // Сортировка
    orderKey: 'fittingCount',
    orderDirection: 'ASC',
    isFittingCountOrder: function(){ return this.get('orderKey') == 'fittingCount' }.property('orderKey'),
    isFittingCountASC: function(){ return this.get('orderKey') != 'fittingCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),
    isRedirectCountOrder: function(){ return this.get('orderKey') == 'redirectCount' }.property('orderKey'),
    isRedirectCountASC: function(){ return this.get('orderKey') != 'redirectCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),
    isWardrobeCountOrder: function(){ return this.get('orderKey') == 'wardrobeCount' }.property('orderKey'),
    isWardrobeCountASC: function(){ return this.get('orderKey') != 'wardrobeCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),

    // переход на первую стр
    setAllModel: function(){
        var me = this,
            garments = me.get('allGarments');
        me.set('page', 0);
        me.clear();
        me.sortGarments(garments);
        me.calculatePages();
        me.pushObjects(garments.slice(me.get('limit')*me.get('page'), me.get('limit')*(me.get('page')+1)));
    },

    // подсчет и формирование массива страниц
    calculatePages: function(){
        var me = this,
            pages = [],
            pageNum = Math.ceil(me.get('totalCount')/me.get('limit')),
            currentPage = me.get('page');
        for(var i=0; i<pageNum; i++){
            var page = {
                page: i+1,
                active: i == currentPage
            }
            if(i==0 || i==pageNum-1 || Math.abs(i-currentPage)<3){
                pages.push(page);
            } else if(pages[pages.length-1].page != '..'){
                page = {
                    page: '..',
                    disabled: true,
                    active: false
                }
                pages.push(page);
            }
        }
        me.set('pages', pages);
    },

    // сортировка предметов по установленным параметрам
    sortGarments: function(garments){
        var me = this,
            orderKey = me.get('orderKey'),
            orderDirection = me.get('orderDirection');
        garments.sort(function(a,b){
            if(a.get(orderKey) < b.get(orderKey)){
                return orderDirection == 'ASC' ? -1 : 1;
            }
            if(a.get(orderKey) > b.get(orderKey)){
                return orderDirection == 'ASC' ? 1 : -1;
            }
            return 0;
        })
    },

    // построение запроса после изменения фильтров, вызов запроса
    changedFilter: function(){
        var me = this,
            model = App.GarmentModel.create({}),
            to = me.get('parent_controller').get('dateTo'),
            from = me.get('parent_controller').get('dateFrom');

        model.find({
                api: 'report_audit_total_list_all',
                data: {
                    article: me.get('articleQuery'),
                    categories: me.get('selected_category').get('id'),
                    from: from,
                    to: to
                }
            }).then(function(garments){
                me.set('allGarments', garments);
                me.setAllModel();
            });
    },

    actions:{
        // событие смены периода
        changedPeriod: function(params){
            this.set('selected_period', params[0]);
            this.changedFilter();
        },
        // собтие смены категории
        changedCategory: function(params){
            this.set('selected_category', params[0]);
            this.changedFilter();
        },
        // событие на смену поля или направления сортировки
        changedOrder: function(key){
            var me= this,
                orderKey = me.get('orderKey'),
                orderDirection = me.get('orderDirection');
            if(orderKey && orderKey == key){
                orderDirection = orderDirection == 'ASC' ? 'DESC' : 'ASC';
            } else {
                orderKey = key;
                orderDirection = 'ASC';
            }
            me.set('orderKey', orderKey);
            me.set('orderDirection', orderDirection);
            me.setAllModel();
        },
        // событие смены значения поля поиска по артиклу
        changedArticleQuery: function(){
            this.changedFilter();
        },
        // смена страницы
        changePage: function(page, skip){
            if(skip) return;
            var me = this,
                garments = me.get('allGarments');
            me.set('page', page-1);
            me.calculatePages();
            me.clear();
            me.pushObjects(garments.slice(me.get('limit')*me.get('page'), me.get('limit')*(me.get('page')+1)));
        },
        clearSelected: function(){
            var me = this;
            me.set('allChecked', false);
            me.get('allGarments').filterBy('selected', true).forEach(function(m){
                m.set('selected', false);
            });
        }
    },

    // observed attributes
    allCheckedObserved: function(){
        var me = this,
            input = $(event.target).parents('label').find('input:checkbox');
        me.get('allGarments').slice(me.get('limit')*me.get('page'), me.get('limit')*(me.get('page')+1))
            .forEach(function(m){
                m.set('selected', input.is(':checked'));
        });
    }.observes('allChecked')
});