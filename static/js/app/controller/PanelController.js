App.PanelController =Ember.Controller.extend({
    activeMenu: '',
    isActiveMenuSettings: function(){
        return this.get('activeMenu') == 'settings';
    }.property('activeMenu')
});