App.PanelCatalogCatalogTreeController = Ember.ArrayController.extend({

    needs: ['panelCatalogGarment'],
    //обновлять дерево при каждом раскрытии
    killAlways: true,

    //индикация процесса загрузки
    loadProcess: false,

    //раскрывать существующие дочерние узлы ( применяется при поиске - когда
    // удобно отобразить результат выборки в дереве каталогов
    isOpenExisting: false,

    init : function(){
        var me = this;
        App.CategoryModel.create().find().then(function(m){
            me.set('content', m);
            me.set('store', m);
        }, function(error){
               //handler ошибки загрузки
            }
        );
       // me.initCatalogTree();
    },

    /**
     * инициализация анимации дерева (DEPRECATED)
     * */
    initCatalogTree : function(){
        var isTreeInit = App.PanelCatalogCatalogTreeController.isTreeInit;
        if (isTreeInit)
            return;
        else
            App.PanelCatalogCatalogTreeController.isTreeInit=true;

        //Здесь дерево еще не отрисовано
       $(document).on('click', '.listTree .node', function(e){
        var node = $(e.target);
        if (node.hasClass('level_0')) {
            node.next('ul').toggle(200);
            node.toggleClass('selected');
        }
        else {
            if (node.hasClass('level_1')){
                node.next('.add_block').toggle(200);
                node.toggleClass('selected');
            }
        }
      })
    },

    /** Анимация лоадера
     *
     * @show - true -показать/ false - скрыть
     * @category - id объект на котором загрузка
     * */
    loadProgress: function(show, obj_id) {
        var me = this;
            me.set('loadProcess', show);
//
//            el = $('.listTree .node[data-id='+ obj_id +']').parents('li').find('ul .primary_loading');
//
//        if( show) {
//            el.show();
//        }
//        else {
//            el.hide();
//        }
    },

    /**
     * Переключение узла дерева в DOM
     * @category - категория которую следует переключить/ или массив категорий которые нужно открыть
     */
    toggleCategoryEl: function(category){

        //функция обработки узла
        var procNode = function(node, tog){

            if (node.hasClass('level_0')) {
                node.next('ul').toggle(200);
                if (tog)
                    node.toggleClass('selected');
            }
            else {
                if (node.hasClass('level_1')){
                    node.next('.add_block').toggle(200);
                    if (tog)
                        node.toggleClass('selected');
                }
            }
        }

        if (Array.isArray(category)) {
            //передан массив id категорий для раскрытия
            var search= []
            category.forEach(function(itm){
                search.push('.listTree .node[data-id='+ itm+']');
            });

            search = search.join(',');
            var nodes = $(search);
            nodes.each(function(node){
                procNode(node, false);
            });
        }
        else {
            //раскрыть 1 узел
            var node = $('.listTree .node[data-id='+ category.get('id')+']');
            procNode(node, true);
        }
    },


    actions: {

         /** создание новой одежды
         * @parentCategory - родительская категория одежды
         * */
        createGarment: function(parentCategory){
             //NOTE transitionToRoute - не срабатывает в случае локации уже на данном url
             if (location.hash=="#/panel/catalog/garment/new"){
                var garmentController = this.get('controllers.panelCatalogGarment'),
                    defaultModel = garmentController.get('model').get('defaultModel');

                debugger;

                garmentController.set('model', defaultModel.clone());
                garmentController.get('model').set('category', parentCategory);
                garmentController.get('model').set('isChange', false);

                garmentController.get('model').set('defaultModel', defaultModel.clone());
                garmentController.resetValidation();
                garmentController.resetProperties();
             }
             else {
                 this.set('tempParentCategory', parentCategory);
                 this.transitionToRoute('panel.catalog.garment.new');
             }
        },

        /**
         * раскрытие/закрытие вложенной категории
         * */
        toogleSubcategory: function(id){
            var me = this,
                category = me.findBy('id', id);

            category.set('loading', true);

            //TODO т.к. переключили поиск можно не учитывать?
            me.set('isOpenExisting', false);

            me.toggleCategoryEl(category);

            //раскрыта категория
            if (!category.get('isOpen')) {
                category.set('isOpen', true);

                //есть ли у категории дочерние категории и нужно уничтожать дочерние - как обновление?
                if ((!category.get('children').length && me.killAlways) || (!category.get('children').length && !me.killAlways)){
                        //me.parentToggledId = id;
                        //показать индикатор загрузки
                        me.loadProgress(true, id);

                        //загрузка дочерних категорий
                        App.CategoryModel.create().find({data: {parents: id} }).then(function(child_data){
                            if (child_data && child_data.length) {
                                child_data[child_data.length-1].set('lastChild', true);
                            }

                            var notLots = child_data.length<=0?true:false;
                            category.set('isNotLots', notLots);

                            category.set('children', child_data);
                            category.set('childCount', child_data.length);

                            me.loadProgress(false, id);
                            category.set('loading', false);
                        }, function(error){
                            me.loadProgress(false, id);
                            category.set('loading', false);
                        });
                }
                else {
                    category.set('loading', false);
                }
            }
            else {
                category.set('isOpen', false);
                category.set('loading', false);

                //очищать всегда
                if (me.killAlways) {
                    category.set('children', []);
                }
            }
        },

        /** раскрытие/закрытие списка одежды выбранной подкатегории */
        toogleGarments: function(sub_category){
             var me = this,
                 id = sub_category.id,
                 parentId = sub_category.parent.id,
                 topCategory = me.findBy('id', parentId),
                //поиск дочерней категории
                childCategory = topCategory.children.findBy('id', id);

             //TODO т.к. переключили поиск можно не учитывать?
            me.set('isOpenExisting', false);

             me.toggleCategoryEl(childCategory);


            if (!childCategory.get('isOpen')) {
                childCategory.set('isOpen', true);

                //индикатор загрузки
                me.loadProgress(true, id);
                childCategory.set('loading', true);

                App.GarmentModel.create().find({api: 'garment_list_all',
                                            data:{ categories : id} }).then(function(garment_data){

                         var notLots = garment_data.length<=0?true:false;
                         childCategory.set('isNotLots', notLots);

                        //обновить дочернюю категорию новой одеждой
                        childCategory.set('children', garment_data);
                        childCategory.set('childCount', garment_data.length);

                        me.loadProgress(false, id);
                        childCategory.set('loading', false);

                     }, function(error_data){
                         me.loadProgress(false, id);
                         childCategory.set('loading', false);
                     }
                );

            }
            else {
                childCategory.set('isOpen', false);
            }
        }
    },

    /**
     * Установка списка товаров в дерево каталогов (применяется после поиска, фильтрации)
     * */
    setGarmentList: function(list) {
        var me = this,
            oldStore = me.get('content'),
            currentStore = [];

            oldStore.forEach(function(item){
            //опустошить детей
            item.get('children').clear();
            item.set('isOpen', false);
            currentStore.push(item);
        });

        list.forEach(function(item){
            var rootCat =  App.CategoryModel.create(item.category.parent),
                currentCat = App.CategoryModel.create(item.category),
                rootReal = currentStore.findBy('id', rootCat.get('id')),
                categoryReal = null,
                garmentItem = App.GarmentModel.create(item);

            categoryReal = rootReal.get('children').findBy('id', currentCat.get('id'));

            if ( !categoryReal ){
                currentCat.set('lastChild', true);

                if (rootReal.get('children').length>0){
                    var lena = rootReal.get('children').length;
                    (rootReal.get('children')[lena-1]).set('lastChild', false);
                }

                rootReal.get('children').push(currentCat);


                categoryReal = rootReal.get('children').findBy('id', currentCat.get('id'));
            }
            categoryReal.get('children').push(garmentItem);
        });

        me.clear();
        me.pushObjects(currentStore);

        //me.set('content', currentStore);
//        me.set('store', currentStore);

        //раскрыть результат в дереве
        me.openExisting();
    },

    //открытие существующих узлов с детьми
    openExisting: function() {
        var me = this,
            store = me.get('content'),
            openArray = [];

        me.set('isOpenExisting', true);

        store.forEach(function(item){
            if (item.children.length>0){
                item.set('isOpen', true);
                openArray.push(item.get('id'));
                item.children.forEach(function(chItem){
                   if (chItem.children && chItem.children.length>0) {
                       chItem.set('isOpen', true);
                       openArray.push(chItem.get('id'));
                   }
                });
            }

        });
        //раскрыть в DOM
        //me.toggleCategoryEl(openArray);
    },


    loadBar: function(show){
        var me = this,
            panel = $('.catalog_lot_panel .panel-body');
        if (show) {
            panel.prepend('<img id="treeLoader" class="primary_loading tree_lev_1 text-center" src="/static/img/elements/ajax-loader.gif">');
        }
        else {
            panel.find('#treeLoader').remove();
        }
    },

    /**
     *  Раскрыть дерево
     */
    openCatalogTree: function(){
        var me = this;
        me.set('isOpenExisting', false);

        me.loadBar(true);
        //получитьб полное дерево каталогов
        App.CategoryModel.create().find({data: {depth:3} }).then(function(child_data){
            //получить полный список одежды в категориях
            //сформировать строку запроса - idки категорий
            var csid = [];
            for (var i=0; i<child_data.length; i++){
                var rootDepth = child_data[i],
                    childCategories = [];
                rootDepth.get('children').forEach(function(itm){
                    csid.push(itm.id);
                    itm =  App.CategoryModel.create(itm);
                    childCategories.push(itm);
                });
                rootDepth.set('children', childCategories);
            }
            var findGar = csid.join(',');
            App.GarmentModel.create().find({api: 'garment_list_all',
                                            data: {categories:findGar}}).then(function(garment_data){
                    for (var i=0; i<child_data.length; i++){
                        var rootDepth = child_data[i];
                        rootDepth.children.forEach(function(itm){
                            //поиск загруженной одежды категории
                            var garmList = garment_data.filter(function(citm){
                                return citm.category.id==itm.id;
                            });
                            itm.set('children', garmList);
                         });
                    }
                    me.clear();
                    me.pushObjects(child_data);
                    //раскрыть результат в дереве
                    me.openExisting();
                    me.loadBar(false);
            });

        }, function(error){
            me.loadProgress(false, id);
            me.loadBar(false);
            category.set('loading', false);
        });


    },
    /** закрыть дерево */
    closeCatalogTree: function(){

        var me = this;
        me.set('isOpenExisting', false);
        me.clear();
        me.init();
    }

});

//флаг инициализации анимации раскрытия
App.PanelCatalogCatalogTreeController.isTreeInit = false;