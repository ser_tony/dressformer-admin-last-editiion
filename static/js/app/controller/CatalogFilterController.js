/**
 * Контроллер Фильтр каталогов
 * здесь сохраняется состояние фильтров поиска, выборки одежды из модуля
 * каталог
 *
 * */

App.PanelCatalogCatalogFilterController = Ember.Controller.extend({
    init: function(){
    },

    needs: ['panelCatalogCatalogTree'],

    //артикул по умолчанию
    findWord: '',

    //флаг раскрытия каталога
    isOpenedCatalogTree: false,

    isFindEmpty: function() {
        var me = this;
        if (!me.findWord || me.findWord=='')
            return true;
        return false;
    },

    actions: {
        //поиск одежды по артикулу - возвращает список совпадающий
        findByArticle: function(){
            var me = this;
            if (!me.get('findWord') || me.get('findWord')=='')
                return;

            App.GarmentModel.create().find({api: 'garment_get_by_article_list_all',
                                       data: {article : '%' + me.get('findWord') + '%'}
            }).then(function(garmentList){
                    //обновить список каталога
                    me.get('controllers.panelCatalogCatalogTree').setGarmentList(garmentList);
                    //+ раскрыть списки
                }, function(error){

                });
        },

        //раскрыть(скрыть) все дерево каталога
        triggerOpenCatalogTree: function() {
            var me = this;

            if (me.get('isOpenedCatalogTree') ) {
                me.get('controllers.panelCatalogCatalogTree').closeCatalogTree();
                me.set('isOpenedCatalogTree', false);
            }
            else {
                me.get('controllers.panelCatalogCatalogTree').openCatalogTree();
                me.set('isOpenedCatalogTree', true);
            }
        }
    }

});