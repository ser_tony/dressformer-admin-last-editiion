App.PanelSettingsPartnerSeController = Ember.Controller.extend({
    needs: ['panelSettingsPartner'],
    partner: Ember.computed.alias("controllers.panelSettingsPartner"),
    isGenderMale: function(){return this.get('partner').get('isGenderMale');}.property('partner.gender'),
    isGenderFemale: function(){return this.get('partner').get('isGenderFemale');}.property('partner.gender'),

    init: function(){
        var me = this;
        me._super();
        console.log(me.get('isGenderMale'), me.get('isGenderFemale'));
    },

    actions: {
        changeGender: function(gender){
            console.log('==', this.get('isGenderMale'), this.get('isGenderFemale'), gender);
            this.get('partner').set('gender', gender);
            console.log(this.get('isGenderMale'), this.get('isGenderFemale'), gender);
        }
    }

});