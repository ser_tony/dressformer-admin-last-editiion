App.PanelSettingsPartnerController = Ember.Controller.extend({
    dates: function(){
        var dates = [],
            last_date = new Date(this.get('selected_year'), this.get('selected_month').value+1, 0).getDate();
        for(var i=1; i<=last_date; i++){
            dates.push(i);
        }
        return dates;
    }.property('selected_month', 'selected_year'),
    months: [],
    years: [],
    selected_year: 2000,
    selected_month: null,
    selected_date: 1,
    term: false,
    gender: 'male',
    isGenderMale: function(){ return this.get('gender') == 'male'; }.property('gender'),
    isGenderFemale: function(){ return this.get('gender') == 'female'; }.property('gender'),
    init: function(){
        var me = this;
        me._super();
        for(var i=1970; i<(new Date()).getFullYear()-5; i++){
            me.get('years').push(i);
        }
        for(var i=0; i<12; i++){
            me.get('months').push({label:Em.I18n.translations['months.'+i], value: i});
        }
        me.set('selected_month', me.get('months')[0]);
    },
    dateOfBithday: function(){
        var birthday = this.get('model').get('birthday');
        if(birthday){
            (new Date(birthday)).getDate();
        }
        return null;
    }.property('model.birthday'),
    monthOfBithday: function(){
        var birthday = this.get('model').get('birthday');
        if(birthday){
            (new Date(birthday)).getMonth();
        }
        return null;
    }.property('model.birthday'),
    yearOfBithday: function(){
        var birthday = this.get('model').get('birthday');
        if(birthday){
            (new Date(birthday)).getFullYear();
        }
        return null;
    }.property('model.birthday'),

    // OBSERVE
    selectedDateObserved: function(){
        console.log('OBSERVED selected_year selected_month');
        var date = this.get('selected_date'),
            last_date = new Date(this.get('selected_year'), this.get('selected_month').value+1, 0).getDate();
        if(date > last_date) this.set('selected_date', last_date);
    }.observes('selected_year', 'selected_month')
});