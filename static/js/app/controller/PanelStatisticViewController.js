App.PanelStatisticViewController = Ember.ObjectController.extend(Em.I18n.TranslateableProperties, {
    needs: ['panelStatistic'],
    parent_controller: Ember.computed.alias("controllers.panelStatistic"),

    garment: App.GarmentModel.create({}),

    totalFittingCount: function(){
        var data = this.get('model').get('statistic');
        if(!data.length) return 0;
        return data.reduce(function (a, i) {
            return {fittingCount: a.fittingCount + i.fittingCount}
        }).fittingCount;
    }.property('model.statistic'),
    totalRedirectCount: function(){
        var data = this.get('model').get('statistic');
        if(!data.length) return 0;
        return data.reduce(function (a, i) {
            return {redirectCount: a.redirectCount + i.redirectCount}
        }).redirectCount;
    }.property('model.statistic'),
    totalWardrobeCount: function(){
        var data = this.get('model').get('statistic');
        if(!data.length) return 0;
        return data.reduce(function (a, i) {
            return {wardrobeCount: a.wardrobeCount + i.wardrobeCount}
        }).wardrobeCount;
    }.property('model.statistic'),

    // периоды для графиков
    fittingDateFrom: moment().format('YYYY-MM-DD'),
    fittingDateTo: moment().format('YYYY-MM-DD'),
    redirectDateFrom: moment().format('YYYY-MM-DD'),
    redirectDateTo: moment().format('YYYY-MM-DD'),
    saveDateFrom: moment().format('YYYY-MM-DD'),
    saveDateTo: moment().format('YYYY-MM-DD'),

    // Данные для построениея графиков ChartView
    fittingDates: [],
    redirectDates: [],
    saveDates: [],
    data_fitting: [],
    data_redirect: [],
    data_save: [],

    nameFittingTranslation: 'panel.statistic.catalog.sort.fittings',
    nameRedirectTranslation: 'panel.statistic.catalog.sort.redirects',
    nameSaveTranslation: 'panel.statistic.catalog.sort.saves',

    // Выпадающий список периода
    selected_period: function(){
        return this.get('parent_controller').get('selected_period');
    }.property('parent_controller'),
    periods: function(){
        return this.get('parent_controller').get('periods');
    }.property('parent_controller'),

    init: function () {
        var me = this;
        me._super();
    },

    drawGraphics: function (fitting, redirect, wardrobe) {
        var me = this,
            fittingDates = [],
            redirectDates = [],
            saveDates = [],
            data = me.get('model').get('statistic'),
            data_fitting = [],
            data_redirect = [],
            data_save = [];
        data.forEach(function (m) {
            if(fitting){
                fittingDates.push(moment(m.get('date')).format('DD.MM'));
                data_fitting.push(m.get('fittingCount'));
            }
            if(redirect){
                redirectDates.push(moment(m.get('date')).format('DD.MM'));
                data_redirect.push(m.get('redirectCount'));
            }
            if(wardrobe){
                saveDates.push(moment(m.get('date')).format('DD.MM'));
                data_save.push(m.get('wardrobeCount'));
            }
        });
        if(fitting){
            me.set('fittingDates', fittingDates);
            me.set('data_fitting', data_fitting);
        }
        if(redirect){
            me.set('redirectDates', redirectDates);
            me.set('data_redirect', data_redirect);
        }
        if(wardrobe){
            me.set('saveDates', saveDates);
            me.set('data_save', data_save);
        }
    },

    changedFilter: function () {
        var me = this,
            to = me.get('parent_controller').get('dateTo'),
            from = me.get('parent_controller').get('dateFrom');

        me.set('fittingDateFrom', from);
        me.set('fittingDateTo', to);
        me.set('redirectDateFrom', from);
        me.set('redirectDateTo', to);
        me.set('saveDateFrom', from);
        me.set('saveDateTo', to);
    },

    // запрос на загрузку данных для графика
    sendRequest: function(fitting, redirect, wardrobe, fromDate, toDate){
        var me = this,
            dataString = [],
            garment = App.GarmentModel.create({});
        if(fitting) dataString.push('fitting');
        if(redirect) dataString.push('redirect');
        if(wardrobe) dataString.push('wardrobe');
        if(!dataString.length) return;
        console.log('--', fromDate, toDate);
        garment.find({
            api: 'report_audit_daily_total_list_all',
            data: {
                garments: me.get('model').get('id'),
                from: fromDate,
                to: toDate,
                data: dataString.join(',')
            }
        }).then(function (m) {
            me.get('model').set('statistic', m);
            me.drawGraphics(fitting, redirect, wardrobe);
        }, function (a) {
            console.log('ERROR REQUEST:', arguments);
        });
    },

    sendRequestFitting: function(fitting, from, to){
        this.sendRequest(fitting, 0, 0, from, to);
    },

    sendRequestRedirect: function(redirect, from, to){
        this.sendRequest(0, redirect, 0, from, to);
    },

    sendRequestSave: function(save, from, to){
        this.sendRequest(0, 0, save, from, to);
    },

    actions: {
        // событие смены периода
        changedPeriod: function (params) {
            this.get('parent_controller').set('selected_period', params[0]);
            this.changedFilter();
        }

    },

    // Observed attributes

    /**
     * Для дат периодов. Когда дата обновляется, то посылаем запрос и перерисовываем определенный график
     */
    fittingDateObserved: function(){
        var me = this;
        Em.run.debounce(me, me.sendRequestFitting, 1, me.get('fittingDateFrom'), me.get('fittingDateTo'), 400);
    }.observes('fittingDateFrom', 'fittingDateTo'),

    redirectDateObserved: function(){
        var me = this;
        Em.run.debounce(me, me.sendRequestRedirect, 1, me.get('redirectDateFrom'), me.get('redirectDateTo'), 400);
    }.observes('redirectDateFrom', 'redirectDateTo'),

    saveDateObserved: function(){
        var me = this;
        Em.run.debounce(me, me.sendRequestSave, 1, me.get('saveDateFrom'), me.get('saveDateTo'), 400);
    }.observes('saveDateFrom', 'saveDateTo')
});