App.AuthController = Ember.Controller.extend({
    isValidLogin: true,
    isValidPassword: true,

    actions:{
        singIn: function(){
            var me= this,
                model = me.get('model'),
                loginInput = $('#authInputLogin'),
                passwordInput = $('#authInputPassword'),
                isError = false,
                btn = $('#btnRegister');
            btn.button('loading');
            // reset validation flags
            me.set('isValidLogin', true);
            loginInput.parent().find('.help-block').empty();
            me.set('isValidPassword', true);
            passwordInput.parent().find('.help-block').empty();
            // validate
            if(!model.get('login').trim()){
                me.set('isValidLogin', false);
                loginInput.parent().find('.help-block').text(Em.I18n.translations['auth.validate.emptyLogin']);
                isError = true;
            }
            if(!model.get('password').trim()){
                me.set('isValidPassword', false);
                passwordInput.parent().find('.help-block').text(Em.I18n.translations['auth.validate.emptyPassword']);
                isError = true;
            }
            // if has not errors send request
            if(!isError){
                 model.authenticate()
                    .then(function(result){
                        console.log(model);
                        $('head').append('<script type="application/javascript" src="/static/js/panel.js"></script>');
                        me.transitionToRoute('panel');
                    },
                    function(error){
                        $('#authInputLogin').parent().find('.help-block').text(error.message);
                    });
            } else {
                btn.button('reset');
            }
        }
    }


});