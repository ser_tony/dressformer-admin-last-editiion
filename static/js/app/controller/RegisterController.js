App.RegisterController = Ember.Controller.extend({
    //public key api (есть еще privat для серверной стороны)
    captcha_public_key_api: "6Lfa--YSAAAAAMtT_FtvWzxH0nJBmCWoGjXNALiV",

    valid: {
        isValidLogin: true,
        isValidPassword: true,
        isValidPasswordConfirm: true,
        isEqualPassword: true,
        isValidRecaptcha: true
    },
    /**
     * Сброс флагов валидации формы
     */
    resetValidation: function(){
        var me = this,
            valid = me.valid,
            validObj = {};
        me.beginPropertyChanges();
        for (var item in valid){
            validObj[item]= true;
        }
        me.set('valid', validObj);
        this.endPropertyChanges();
    },

    subscribe: true,
    confirmTerms: false,

    actions: {
        /**
         * Регистрация пользователя - вызывается из формы
         */
        register: function(){
            var me = this,
                model = me.get('model'),
                loginInput = $('#registerInputLogin'),
                passwordInput = $('#registerInputPassword'),
                passwordConfirmInput = $('#registerInputPasswordConfirm'),
                g_captcha = $('#g_captcha'),
                inputs = [loginInput, passwordInput, passwordConfirmInput, g_captcha],
                buttonRegister = $('#btnRegister'),
                isError = false;

            //очистить поля подсказок валидации
            inputs.forEach(function(item){
                item.parent().find('.help-block').empty();
            });
            //обнуление флагов валидации формы
           me.resetValidation();

            //кнопка в обработке
            buttonRegister.button('loading');

            if(!model.get('email').trim()){
                isError=true;
                me.outError('isValidLogin', loginInput, 'auth.validate.emptyLogin');
            }
            var passw = model.get('password').trim(),
                passwConfirm = model.get('passwordConfirm');
            if(!passw){
                isError =true;
                me.outError('isValidPassword', passwordInput, 'auth.validate.emptyPassword');
            }
            if(!passwConfirm){
                isError =true;
                me.outError('isValidPasswordConfirm',passwordConfirmInput, 'auth.validate.emptyPassword');
            }
            if( (passw && passwConfirm) && (passw!=passwConfirm)){
                    isError =true;
                    me.outError('isEqualPassword',passwordConfirmInput, 'register.validate.notEqualPassw');
            }

            //проверка ввода каптчи
            var recResponse = Recaptcha.get_response(),
                recChallenge = Recaptcha.get_challenge();
            if (!recResponse.trim() || !recChallenge.trim()){
                isError = true;
                me.outError('isValidRecaptcha', g_captcha, 'register.validate.emptyCaptcha');
            }
            else{
                model.set('recaptchaResponse', recResponse);
                model.set('recaptchaChallenge', recChallenge);
            }

            Recaptcha.reload();
            if (!isError){
                model.register().then(function(){
                    //регистрация успешно - редирект на авторизацию
                    me.transitionToRoute('auth');
                }, function(result){
                    me.registerError(result);
                });
            }
            else {
                buttonRegister.button('reset');
            }
        },

        changeTermsAgree: function(state){
            this.set('confirmTerms', state);
        }
    },

    /**
     * Вывод ошибки на этапе валидации параметров необходимых при регистрации
     * @param validateParam - поле в контроллере подлежащее проверке, устанавливается как не прошло валидацию
     * @param formElement - элемент в DOM форме с которым ассоциирован параметр
     * @param errorText - текст ошибки, берется как элемент словаря
     * @param asString - если передан флаг-true, текст береться из аргумента errorText, иначе как ключ словаря
     */
    outError: function(validateParam, formElement, errorText, asString){
        var me = this,
            asString = asString || false,
            strErr = asString ? errorText: Em.I18n.translations[errorText];
        //if(!asString){
            me.set('valid.'+validateParam, false);
        //}
        formElement.parent().find('.help-block').text(strErr);
    },



    /**
     * инициализация функциональности каптчи в форме регистрации
     * если нужно подгрузит код API, после чего  создаст виджет
     */
    initCAPTCHA: function(){
        var me = this;
        var createCaptcha = function(){
            var public_key_api = me.captcha_public_key_api;
            Recaptcha.create(public_key_api, "g_captcha", {
                    theme: "red",
                    //TODO передавать локализацию для каптчи
                    callback: Recaptcha.focus_response_field
                }
            );
        };
        if(typeof Recaptcha =='undefined'){
            //подгрузить скрипт с API Google Recaptcha
            $.getScript('http://www.google.com/recaptcha/api/js/recaptcha_ajax.js', function(){
                createCaptcha();
            });
        }
        else {
            createCaptcha();
        }
    },

    /**
     * Обработка ошибки регистрации
     * @param data - результат возвращается от dressformer API
     * при попытке регистрации пользователя
     */
    registerError: function(data){
        var me = this;
        if (data.message && data.message.trim()) {
            var tmessage = data.message;
            //массив полей формы с возможными ошибками
            fieldErr = [{word: ["'email'","email","login"], el: 'registerInputLogin', param:'isValidLogin'},
                {word: "'password'", el: 'registerInputPassword', param: 'isValidPassword' },
                {word: "captcha", el: 'g_captcha', param: 'isValidRecaptcha'}
            ];
            fieldErr.forEach(function(item){
                if (Array.isArray(item.word)) {
                    item.word.forEach(function(wi){
                        if (tmessage.match(wi)){
                            me.outError(item.param, $("#"+item.el), tmessage, true);
                            return;
                        }
                    });
                }
                else if (tmessage.match(item.word)){
                    me.outError(item.param, $("#"+item.el), tmessage, true);
                    return;
                }
            });
        }
    }
});
