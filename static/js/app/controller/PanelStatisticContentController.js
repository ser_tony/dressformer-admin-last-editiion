App.PanelStatisticContentController = Ember.ArrayController.extend({
    needs: ['panelStatistic'],
    parent_controller: Ember.computed.alias("controllers.panelStatistic"),

    // Статистика по каталогу
    labelCount: '...',
    lotsCount: '...',
    updateDate: '...',
    totalFittingCount: '...',
    totalRedirectCount: '...',
    totalWardrobeCount: '...',

    // значения поля фильтра поиска по артиклу
    articleQuery: function(){
        return this.get('parent_controller').get('articleQuery');
    }.property('parent_controller.articleQuery'),

    // Выпадающий список категорий
    categories: function(){
        return this.get('parent_controller').get('categories');
    }.property('parent_controller.categories'),
    selected_category: function(){
        return this.get('parent_controller').get('selected_category');
    }.property('parent_controller.selected_category'),

    // Пагинация
    pages: [{page: 1, active: true}],
    last_page: true,
    page: 0,
    limit: 10,

    // Выпадающий список периода
    selected_period: function(){
        return this.get('parent_controller').get('selected_period');
    }.property('parent_controller.selected_period'),
    periods: function(){
        return this.get('parent_controller').get('periods');
    }.property('parent_controller.periods'),

    // Сортировка
    orderKey: 'fittingCount',
    orderDirection: 'ASC',
    isFittingCountOrder: function(){ return this.get('orderKey') == 'fittingCount' }.property('orderKey'),
    isFittingCountASC: function(){ return this.get('orderKey') != 'fittingCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),
    isRedirectCountOrder: function(){ return this.get('orderKey') == 'redirectCount' }.property('orderKey'),
    isRedirectCountASC: function(){ return this.get('orderKey') != 'redirectCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),
    isWardrobeCountOrder: function(){ return this.get('orderKey') == 'wardrobeCount' }.property('orderKey'),
    isWardrobeCountASC: function(){ return this.get('orderKey') != 'wardrobeCount' || this.get('orderDirection') == 'ASC' }.property('orderKey', 'orderDirection'),

    // переход на первую стр
    setAllModel: function(){
        var me = this,
            garments = me.get('allGarments');
        me.set('page', 0);
        me.clear();
        me.sortGarments(garments);
        me.calculateStatistic();
        me.calculatePages();
        me.pushObjects(garments.slice(me.get('limit')*me.get('page'), me.get('limit')*(me.get('page')+1)));
    },

    calculateStatistic: function(){
        var me = this,
            labels = [],
            fittingCount,
            redirectCount,
            wardrobeCount,
            garments = me.get('allGarments');
        // дата обновления
        me.set('updateDate', moment().format('YYYY-MM-DD hh:mm:ss'));
        if(garments.length){
            // кол-во лейблов и пр
            garments.forEach(function(i){
                labels.push(i.get('brand'));
            });
            me.set('labelCount', labels.uniq().length);
            me.set('lotsCount', garments.length);
            fittingCount = garments.reduce(function(a,i){return {fittingCount:a.fittingCount+i.fittingCount}}).fittingCount;
            redirectCount = garments.reduce(function(a,i){return {redirectCount:a.redirectCount+i.redirectCount}}).redirectCount;
            wardrobeCount = garments.reduce(function(a,i){return {wardrobeCount:a.wardrobeCount+i.wardrobeCount}}).wardrobeCount;
        } else {
            me.set('labelCount', '...');
            me.set('lotsCount', '...');
            fittingCount = '...';
            redirectCount = '...';
            wardrobeCount = '...';
        }
        me.set('totalFittingCount', fittingCount);
        me.set('totalRedirectCount', redirectCount);
        me.set('totalWardrobeCount', wardrobeCount);
    },

    // подсчет и формирование массива страниц
    calculatePages: function(){
        var me = this,
            pages = [],
            pageNum = Math.ceil(me.get('lotsCount')/me.get('limit')),
            currentPage = me.get('page');
        for(var i=0; i<pageNum; i++){
            var page = {
                page: i+1,
                active: i == currentPage
            }
            if(i==0 || i==pageNum-1 || Math.abs(i-currentPage)<3){
                pages.push(page);
            } else if(pages[pages.length-1].page != '..'){
                page = {
                    page: '..',
                    disabled: true,
                    active: false
                }
                pages.push(page);
            }
        }
        me.set('pages', pages);
    },

    // сортировка предметов по установленным параметрам
    sortGarments: function(garments){
        var me = this,
            orderKey = me.get('orderKey'),
            orderDirection = me.get('orderDirection');
        garments.sort(function(a,b){
            if(a.get(orderKey) < b.get(orderKey)){
                return orderDirection == 'ASC' ? -1 : 1;
            }
            if(a.get(orderKey) > b.get(orderKey)){
                return orderDirection == 'ASC' ? 1 : -1;
            }
            return 0;
        })
    },

    // построение запроса после изменения фильтров, вызов запроса
    changedFilter: function(){
        var me = this,
            now = new Date(),
            model = App.GarmentModel.create({}),
            from = new Date(),
            period = me.get('selected_period').id;
        if(period == 1){
            from = new Date(now);
            from.setHours(0);
            from.setMinutes(0);
            from.setSeconds(1);
        } else if( period == 31){
            from = new Date(now.getFullYear(), now.getMonth(), 1);
        } else if( period == 94){
            var month = now.getMonth()-0;
            if(month < 3){
                month = 0;
            } else if( month < 6){
                month = 3;
            } else if( month < 9){
                month = 6;
            } else {
                month = 9;
            }
            from = new Date(now.getFullYear(), month, 1);
        } else {
            from = new Date(now.getFullYear(), 0, 1);
        }
        model.find({
                api: 'report_audit_total_list_all',
                data: {
                    article: me.get('articleQuery'),
                    categories: me.get('selected_category').get('id'),
                    from: moment(from).format('YYYY-MM-DD'),
                    to: moment(now).format('YYYY-MM-DD')
                }
            }).then(function(garments){
                me.set('allGarments', garments);
                me.setAllModel();
            });
    },

    actions:{
        // событие смены периода
        changedPeriod: function(params){
            this.get('parent_controller').set('selected_period', params[0]);
            this.changedFilter();
        },
        // собтие смены категории
        changedCategory: function(params){
            this.get('parent_controller').set('selected_category', params[0]);
            this.changedFilter();
        },
        // событие на смену поля или направления сортировки
        changedOrder: function(key){
            var me= this,
                orderKey = me.get('orderKey'),
                orderDirection = me.get('orderDirection');
            if(orderKey && orderKey == key){
                orderDirection = orderDirection == 'ASC' ? 'DESC' : 'ASC';
            } else {
                orderKey = key;
                orderDirection = 'ASC';
            }
            me.set('orderKey', orderKey);
            me.set('orderDirection', orderDirection);
            me.setAllModel();
        },
        // событие смены значения поля поиска по артиклу
        changedArticleQuery: function(){
            this.changedFilter();
        },
        // смена страницы
        changePage: function(page, skip){
            if(skip) return;
            var me = this,
                garments = me.get('allGarments');
            me.set('page', page-1);
            me.calculatePages();
            me.clear();
            me.pushObjects(garments.slice(me.get('limit')*me.get('page'), me.get('limit')*(me.get('page')+1)));
        }
    }
});