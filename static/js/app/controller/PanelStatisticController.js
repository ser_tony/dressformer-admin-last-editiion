App.PanelStatisticController = Ember.Controller.extend({
    // значения поля фильтра поиска по артиклу
    articleQuery: '',

    categories: [],
    selected_category: {name: '', id: -1},

    // Выпадающий список периода
    periods: [
        {id: 1, name: Em.I18n.translations['periods.day']},
        {id: 31, name: Em.I18n.translations['periods.month']},
        {id: 94, name: Em.I18n.translations['periods.kvartal']},
        {id: 366, name: Em.I18n.translations['periods.year']}
    ],
    selected_period: function(){ return this.get('periods')[0];}.property(),

    dateFrom: function(){
        var me = this,
            now = new Date(),
            from = new Date(),
            period = me.get('selected_period').id;
        if (period == 1) {
            from = new Date(now);
            from.setHours(0);
            from.setMinutes(0);
            from.setSeconds(1);
        } else if (period == 31) {
            from = new Date(now.getFullYear(), now.getMonth(), 1);
        } else if (period == 94) {
            var month = now.getMonth() - 0;
            if (month < 3) {
                month = 0;
            } else if (month < 6) {
                month = 3;
            } else if (month < 9) {
                month = 6;
            } else {
                month = 9;
            }
            from = new Date(now.getFullYear(), month, 1);
        } else {
            from = new Date(now.getFullYear(), 0, 1);
        }
        return moment(from).format('YYYY-MM-DD');
    }.property('selected_period'),

    dateTo: moment().format('YYYY-MM-DD')
});