App.PanelCatalogGarmentController = Ember.ObjectController.extend({
    // needs: ['panelCatalogController'],
    needs: ['panelCatalog', 'panelCatalogCatalogTree'],
    init: function () {
        this._super();
    },

    photo_readers: {
        front: null,
        back: null,
        preview: null
    },

    /**
     * из модели одежды - сформировать путь к корневому каталогу
     */
    previewFileName: function () {
        if (this.get('model.previewUrl')) {
            var paths = this.get('model.previewUrl').split('/');
            return paths[paths.length - 1];
        }
        return '';
    }.property('model.previewUrl'),

    preSelectedBrand: function () {
        var brand = App.BrandModel.create(this.get('content.brand'));
        //this.get('model').set('brand', brand);
        return brand;
    }.property('content.brand'),

    actions: {
        categorySelected: function (category) {
            var me = this;
            me.setValue('category', category[0]);
        },

        loadBranbs: function (params) {
            var me = this,
                model = params[0],
                Brand = App.BrandModel.create({});
            Brand.find().then(function (brands) {
                console.log(brands);
                model.pushObjects(brands);
            });
        },

        brandSelected: function (params) {
            var me = this,
                selected = params[0];
            me.setValue('brand', selected.getModelValues());
        },

        loadShops: function (params) {
            var me = this,
                model = params[0],
                Shop = App.ShopModel.create({});
            Shop.find().then(function (shops) {
                console.log(shops);
                model.pushObjects(shops);
            });
        },

        shopSelected: function (params) {
            var me = this,
                selected = params[0];
            me.setValue('shop', selected.getModelValues());
        },

        loadColors: function (params) {
            var me = this,
                model = params[0],
                Color = App.ColorModel.create({});
            Color.find().then(function (colors) {
                console.log(colors);
                model.pushObjects(colors);
            });
        },

        colorSelected: function (params) {
            var me = this,
                selected = params[0];
            me.setValue('color', selected.getModelValues());
        },

        selectedViewPhoto: function (view) {
            var label = event.target.tagName == 'LABEL' && $(event.target) || $(event.target).parents('label');
            if (label.hasClass('active')) return;
            label.tab('show');
            label.parents('.btn_views').find('label.active').removeClass('active');
            label.addClass('active');
        },

        showModalImage: function () {
            $('#imgModal').modal();
        },

        closeModalImage: function () {
            $('#imgModal').modal('hide')
        },

        /**
         * Сохранение/Редактирование модели одежды
         * */
        saveGarment: function () {
            var me = this,
                model = me.get('model'),
                tempGarmentId = model.get('id');

            // Если была загружена фотка то достаем base64 и type
            var base64data = me.get('previewData') && me.get('previewData').match(/^data:image\/(.*?);base64,(.*?)$/) || false;
            if(base64data){
                model.set('preview', base64data[2]);
                model.set('previewType', base64data[1]);
                // ДЛЯ ТЕСТА ЗАПОЛНЯЕМ ТЕМ ЖЕ model
                //TODO: решить вопрос с 3D моделью
                model.set('model', base64data[2]);
            }
            var notValidList = model.validate();

            if (notValidList.length) {
                me.validProcess(notValidList);
                return;
            }
            me.get('model').save({
                id: model.get('id'),
                article: model.get('article'),
                internalId: model.get('internalId'),
                description: model.get('description'),
                structure: model.get('structure'),
                preview: model.get('preview'),
                previewType: model.get('previewType'),
                url: model.get('url'),
                categoryId: model.get('category').id,
                shopId: model.get('shop').id,
                brandId: model.get('brand').id,
                colorId: model.get('color').id,
                preview: model.get('preview')
            }, true).then(function (garment) {
                    if (!tempGarmentId) {
                        var model = garment.clone();
                        me.get('controllers.panelCatalogCatalogTree')
                            .get('tempParentCategory')
                            .get('children').unshift(model);
                        me.transitionToRoute('panel.category.garment.edit', model);
                        return;
                    }

                    if (me.get('realModel').get('category').id != garment.get('category').id) {

                        me.get('realModel').destroy(true).then(function () {
                            var pCat = me.get('controllers.panelCatalogCatalogTree').get('content')
                                .findBy('id', me.get('realModel').get('category').parent.id);
                            if (!pCat) return;
                            var cCat = pCat.get('children').findBy('id', me.get('realModel').get('category').id);
                            if (!cCat) return;
                            cCat.set('children', cCat.get('children').without(me.get('realModel')));
                        });
                    } else {
                        me.get('realModel').setModelValues(garment.getModelValues());
                    }
                    me.set('wasChanged', false);
                }, function (error) {
                    console.log(error);
                });

            //TODO валидация модели
        },

        //отмена сохранения модели
        cancelGarment: function () {
            var me = this;
            me.set('wasChanged', false);
            me.get('model').setModelValues(me.get('realModel').getModelValues());
            me.get('model').set('isChange', false);
            // clear photo attributes
            me.set('frontError', false);
            me.set('frontLoading', false);
            me.set('frontData', false);

            me.set('backError', false);
            me.set('backLoading', false);
            me.set('backData', false);

            me.set('previewError', false);
            me.set('previewLoading', false);
            me.set('previewData', false);
        },

        deletePhoto: function (label) {
            var me = this,
                model = me.get('model');
            model.set(label, '');
        },

        abortPhoto: function (label) {
            var me = this;
            me.get('photo_readers')[label].abort();
            me.set(label + 'Loading', false);
        },

        fileSelected: function (photo) {
            var me = this,
                file = event.target.files[0],
                reader = me.get('photo_readers')[photo];
            me.set(photo + 'Error', false);
            if (!file.type.match(/image.*/)) {
                me.set(photo + 'Error', Em.I18n.translations['panel.catalog.garment.form.photo.load.error.only.images']);
                return;
            }
            function abortRead() {
                reader.abort();
            }

            function errorHandler(evt) {
                switch (evt.target.error.code) {
                    case evt.target.error.NOT_FOUND_ERR:
                        me.set(photo + 'Error', Em.I18n.translations['panel.catalog.garment.form.photo.load.error.only.images']);
                        break;
                    case evt.target.error.NOT_READABLE_ERR:
                        me.set(photo + 'Error', Em.I18n.translations['panel.catalog.garment.form.photo.load.error.file_not_found']);
                        break;
                    case evt.target.error.ABORT_ERR:
                        break; // noop
                    default:
                        me.set(photo + 'Error', Em.I18n.translations['panel.catalog.garment.form.photo.load.error.occurred_reading']);
                }
            }

            function updateProgress(evt) {
                var attr = photo + 'Loading';
                // evt is an ProgressEvent.
                if (evt.lengthComputable) {
                    var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
                    // Increase the progress bar length.
                    if (percentLoaded < 100) {
                        me.set(attr, percentLoaded + '%');
                    }
                }
            }

            reader = new FileReader();
            reader.onerror = errorHandler;
            reader.onprogress = updateProgress;
            reader.onloadstart = function (e) {
                me.set(photo + 'Loading', '0%');
            };
            reader.onload = function (e) {
                // Ensure that the progress bar displays 100% at the end.
                me.set(photo + 'Loading', false);
                // read to base64
                me.set(photo + 'Data', e.target.result);
                console.log('onload', e, e.target.result);
                me.set(photo + 'LoadedFileName', file.name);
                me.set(photo + 'SizePhoto', (file.size / 1000 / 1000).toFixed(2) + ' Mb');

            }

            // Read in the image file as a binary string.
            reader.readAsDataURL(file);
        }
    },

    /**
     * Валидатор обработчик
     *
     * @list - список полей-флагов с непройденной валидацией
     * */
    validProcess: function (list) {
        var me = this,
            valid = me.valid,
            validFinal = {};
        me.beginPropertyChanges();
        for (var item in valid) {
            var validObj = {};
            validObj.msg = valid[item]['msg'];

            if (list.indexOf(item) >= 0) {
                validObj['isValid'] = false;
                validObj['text'] = Em.I18n.translations[ validObj['msg'] ];
            }
            else {
                validObj['isValid'] = true;
                validObj['text'] = '';
            }
            validFinal[item] = validObj;
        }
        me.set('valid', validFinal);
        this.endPropertyChanges();
    },

    /** Система размеров */
    countrySizes: [
        {id: 1, 'name': 'Международная'},
        {id: 2, 'name': 'Россия'}
    ],


    modelObserver: function (controller) {
        var me = this;
        if (controller.get('content').get('isChange')) {
            // Show button Save
            me.set('wasChanged', true);
        } else {
            me.set('wasChanged', false);
        }
    }.observes('content.isChange'),


    /** установка значения поля в моделе контроллера */
    setValue: function (fname, fvalue) {
        var me = this,
            obj = fvalue,
            model = me.get('model');
        model.set(fname, obj);
        me.set('wasChanged', true);
    },

    /** флаг - изменения модели*/
    wasChanged: false,

    resetProperties: function () {
        this.set('wasChanged', false);
    },

    /** Флаги валидация формы и ключ сообщения в словаре*/
    valid: {
        article: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.article'
        },
        url: {
            isValid: true,
            msg: ''
        },
        category: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.category'
        },

        structure: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.structure'
        },

        shop: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.shop'
        },
        color: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.color'
        },
        brand: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.brand'
        },
        preview: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.preview'
        },
        model: {
            isValid: true,
            msg: 'panel.catalog.garment.form.validate.model'
        }
    },

    /**
     * Сброс флагов валидации формы
     */
    resetValidation: function () {
        var me = this,
            valid = me.valid,
            validFinal = {};
        me.beginPropertyChanges();
        for (var item in valid) {
            var validObj = {};
            validObj.msg = valid[item]['msg'];
            validObj.isValid = true;
            validObj.text = '';
            validFinal[item] = validObj;
        }
        me.set('valid', validFinal);
        this.endPropertyChanges();
    },


    /**
     * ------------------- PROPERTIES ------------------
     */
    styleLoadingPreviewPhoto: function () {
        if (!this.get('previewLoading')) return 'width: 0%;';
        return 'width: ' + this.get('previewLoading');
    }.property('previewLoading')


});