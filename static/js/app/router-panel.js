App.Router.map(function(){
    this.resource('panel', { path: '/panel' }, function(){
        this.resource('panel.catalog', {path: '/catalog/'}, function(){
            this.resource('panel.catalog.garment', {path: '/garment'}, function(){
                this.route('new');
                this.resource('panel.catalog.garment.edit', {path: '/edit/:id'});
            });

        });
        this.resource('panel.statistic', {path: '/statistic/'}, function(){
            this.route('content');
            this.resource('panel.statistic.view', {path: '/view/:id'})
            this.route('audience');
            this.route('report');
        });
        this.route('balance');
        this.resource('panel.settings', { path: '/settings/'}, function(){
            this.resource('panel.settings.partner', { path: '/partner/'}, function(){
                this.route('person');
                this.route('se');
                this.route('individual');
            });
            this.route('account');
        });
    });
});