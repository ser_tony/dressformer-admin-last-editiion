# -*- coding: UTF-8 -*-
import httplib
import urllib
import sys
from tornado import template
import tornado.ioloop
import tornado.httpserver
from tornado.options import define, options
import tornado.web
import os

define("port", default=8081, help="run on the given port", type=int)
define("package", default=False, help="generate scripts", type=bool)
define("compress", default=False, help="compress scripts", type=bool)
define("test", default=False, help="test scripts", type=bool)

AppName = 'App'


def joinScripts(scripts):
    out = ''
    utils = ''
    static_path = os.path.join(os.path.dirname(__file__), "static", "js", "app")

    # load utils
    if scripts.get('utils'):
        with open(os.path.join(static_path, scripts.get('utils') + '.js'), 'rt') as f:
            utils += f.read()

    # load Models
    for model in scripts.get('models', []):
        with open(os.path.join(static_path, 'model', model + '.js'), 'rt') as f:
            out += f.read()

    # load Router
    with open(os.path.join(static_path, scripts.get('router') + '.js'), 'rt') as f:
        out += f.read()

    # load Routes
    for route in scripts.get('routes', []):
        with open(os.path.join(static_path, 'route', route + '.js'), 'rt') as f:
            out += f.read()

    # load Controllers
    for controller in scripts.get('controllers', []):
        with open(os.path.join(static_path, 'controller', controller + '.js'), 'rt') as f:
            out += f.read()

    # load Views
    for view in scripts.get('views', []):
        with open(os.path.join(static_path, 'view', view + '.js'), 'rt') as f:
            out += f.read()

    # load Components
    for component in scripts.get('components', []):
        with open(os.path.join(static_path, 'component', component + '.js'), 'rt') as f:
            out += f.read()

    # load Templates
    for template in scripts.get('templates', []):
        if "/" in template:
            dirs = template.split('/')
            paths = [static_path, 'tpl'] + dirs[:-1]
            path = os.path.join(*paths)
            path = os.path.join(path, dirs[-1] + '.hbs')
            with open(path, 'rt') as f:
                out += ('Ember.TEMPLATES["' + template + '"] = Ember.Handlebars.compile("%s");' %
                        f.read().replace("\r", "").replace("\n", "").replace('"', '\\"')) + "\n"
                # if component register
                if dirs[0] != 'components':
                    continue
                name = "".join([n.capitalize() for n in dirs[-1].split('-')])
                out += "App.registerComponent('"+dirs[-1]+"');"
        else:
            with open(os.path.join(static_path, 'tpl', template + '.hbs'), 'rt') as f:
                out += ('Ember.TEMPLATES["' + template + '"] = Ember.Handlebars.compile("%s");' %
                        f.read().replace("\r", "").replace("\n", "").replace('"', '\\"')) + "\n"

    # load test scripts
    if options.test and scripts.get('tests', []):
        for test in scripts.get('tests', []):
            with open(os.path.join(static_path, 'test', test + '.js'), 'rt') as f:
                out += f.read()

    if scripts.get('app'):
        with open(os.path.join(static_path, scripts.get('app') + '.js'), 'rt') as f:
            app = f.read()
            out = utils + app.replace('{{scripts}}', out)
    return out


def joinStartupJS():
    sctipts = {
        'utils': 'utils',
        'app': 'app',
        'router': 'router-startup',
        'models': ['BaseModel', 'UserModel'],
        'routes': ['ApplicationRoute', 'IndexRoute', 'AuthRoute', 'RegisterRoute'],
        'controllers': ['ApplicationController', 'AuthController', 'RegisterController'],
        'views': ['AuthView', 'SocialPanelView', 'RegisterView', 'LanguagePanelView'],
        'templates': ['auth', 'register', 'socialPanel', 'languagePanel'],
        'tests': options.test and ['testStartup'] or []
    }
    return joinScripts(sctipts)


def joinPanelJS():
    sctipts = {
        'router': 'router-panel',
        'models': ['CategoryModel', 'GarmentModel', 'BrandModel', 'ShopModel', 'ColorModel'],
        'routes': [
            'PanelRoute', 'PanelCatalogRoute', 'PanelStatisticRoute', 'PanelBalanceRoute', 'PanelSettingsRoute',
            'PanelSettingsPartnerRoute', 'PanelCatalogGarmentRoute', 'PanelCatalogGarmentEditRoute',
            'PanelStatisticRoute', 'PanelStatisticLoaderRoute', 'PanelStatisticContentRoute',
            'PanelStatisticReportRoute',
            'PanelCatalogGarmentNewRoute',
            'PanelStatisticViewRoute'
        ],
        'controllers': [
            'PanelController',
            'PanelCatalogController', 'CatalogTreeController', 'CatalogFilterController',
            'PanelCatalogGarmentController',
            'PanelSettingsPartnerController', 'PanelSettingsPartnerIndividualController',
            'PanelSettingsPartnerSeController',
            'PanelStatisticContentController',
            'PanelStatisticController',
            'PanelStatisticViewController', 'PanelStatisticReportController'
        ],
        'views': [
            'BlockPanelView', 'PanelIndexView', 'PanelCatalogView', 'PanelBalanceView',
            'PanelSettingsView', 'TreeNodeCategoryView',
            'PanelCategoryContentView', 'ChartView'
        ],
        'templates': [
            'panel', 'menu',
            'panel/balance',
            'panel/block_panel', 'panel/index', 'panel/catalog', 'panel/statistic',
            'panel/catalog/index',
            'panel/catalog/catalog_tree',
            'panel/catalog/catalog_filter',
            'panel/catalog/garment',
            'panel/settings',
            'panel/settings/partner', 'panel/settings/partner/individual',
            'panel/statistic/content', 'panel/settings/partner/se', 'panel/settings/partner/person',
            'panel/statistic/view', 'panel/statistic/report',

            #components templates
            'components/select-field'
        ],
        'components': [
            'SelectFieldComponent'
        ]
    }
    return joinScripts(sctipts)


def buildScripts():
    static_path = os.path.join(os.path.dirname(__file__), "static", "js")
    startup_path = os.path.join(static_path, 'startup.js')
    panel_path = os.path.join(static_path, 'panel.js')
    startup_data = joinStartupJS()
    panel_data = joinPanelJS()
    with open(startup_path, 'wt') as f:
        f.write(startup_data)
    with open(panel_path, 'wt') as f:
        f.write(panel_data)
    if options.compress:
        for file_item in [[startup_path, startup_data], [panel_path, panel_data]]:
            params = urllib.urlencode([
                ('js_code', file_item[1]),
                ('compilation_level', 'SIMPLE_OPTIMIZATIONS'),
                ('output_format', 'text'),
                ('output_info', 'compiled_code'),
            ])

            # Always use the following value for the Content-type header.
            headers = {"Content-type": "application/x-www-form-urlencoded"}
            conn = httplib.HTTPConnection('closure-compiler.appspot.com')
            conn.request('POST', '/compile', params, headers)
            response = conn.getresponse()
            data = response.read()
            with open(file_item[0], 'wt') as f:
                f.write(data)
            conn.close()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        f = 'index.html'
        if options.test:
            f = 'index_test.html'
        self.write(open(f, 'rt').read())
        self.finish()


class TestDraft(tornado.web.RequestHandler):
    def get(self):
        self.write(open('test_draft.html', 'rt').read())
        self.finish()


class BaseJSHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/x-javascript')
        self.write(self.getScripts())


class StartupHandler(BaseJSHandler):
    def getScripts(self):
        return joinStartupJS()


class PanelHandler(BaseJSHandler):
    def getScripts(self):
        pass


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),

            # Начальный скрипт с авторизацией
            (r"/compress/js/app/startup.js", StartupHandler),

            # Cкрипт после авторизации
            (r"/compress/js/app/panel.js", PanelHandler),

            (r"/test_draft", TestDraft),

        ]
        settings = dict(
            static_path=os.path.join(os.path.dirname(__file__), "static")
        )
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    tornado.options.parse_command_line(sys.argv)
    buildScripts()
    if not options.package:
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
